@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
        </div>
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Address</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              
              <form method="POST" action="/updateAddress/{{ $data->id }}">
                  {{ csrf_field() }}
                <div class="pl-lg-4">
                  <div class="row">

                    <input type="hidden" value="{{ $id }}" name="userID">
                    <input type="hidden" value="{{ $data->id }}" name="AddressID">


          <div class="col-md-6">
          <div class="form-group">
              <label class="form-control-label" for="input-country">Addres Type</label>
            <select required name="addressType" class="form-control form-control-alternative">
              @if($data->addressOF=='Home')
              <option value="Home" selected>Home</option>
              <option value="Office">Office</option>
              <option value="Other">Other</option>
              @elseif($data->addressOF=='Office')
              <option value="Home">Home</option>
              <option value="Office" selected>Office</option>
              <option value="Other">Other</option>
              @elseif($data->addressOF=='Other')
              <option value="Home">Home</option>
              <option value="Office" >Office</option>
              <option value="Other" selected>Other</option>

              @endif
            </select>

          </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
          <label class="form-control-label" for="input-country">Address</label>
          <input  value="{{$data->addressBody}}"  type="text" id="input-postal-code" name="address" class="form-control form-control-alternative" placeholder="Address">
        </div>
      </div>


                    </div>
                </div>
                <!-- Save Button -->
                  <div class="col-12 text-right">
                <input class="btn btn-default" type="submit" value="Update"></input>
                </div>
              </form>


            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
