@extends('layouts.website')
@section('content')
        <main>
            <section class="section pb-0">
                <div class="row row-grid">
                    <div class="col-lg-12 mb-lg-auto p-0" style="position: relative">
                        <img src="{{ asset('assets/img/theme/home-drawing.svg') }}" class="col-md-12 p-0">
                        <div class="col-md-7"></div>
                        <div id="carousel_example" class="carousel slide col-md-5 anim carouselHide" data-ride="carousel" style="padding-top: 5%;position: absolute; right: 10%; top:30%">
                            <div class="carousel-inner" style="padding-bottom: 8%">
                              <?php
                              $sliders = \App\Slider::where('status', 'Active')->get();
                              ?>
                              @foreach($sliders as $value)
                              <div class="carousel-item active">
                                  <div class="row">
                                    <?php
                                    $contentUrl = "/slider/"."/".$value->content;
                                    $content = Storage::get($contentUrl);
                                    ?>
                                    {!!html_entity_decode($content)!!}
                                  </div>
                              </div>
                              @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section section-lg" id="about">
                <div class="container">
                    <div class="row row-grid justify">
                        <div class="col-lg-12 text-center">
                          <h2 class="display-3 revealer para">About Us</h2>
                          <?php
                          $ourinfo = \App\Ourinformation::all();
                          ?>
                          @if(!$ourinfo->isEmpty())
                          <?php
                          $ourinfo = \App\Ourinformation::first();
                          $aboutusUrl = "/aboutus/"."/".$ourinfo->aboutus;
                          $aboutus = Storage::get($aboutusUrl);
                          ?>
                          {!!html_entity_decode($aboutus)!!}
                          @endif
                        </div>
                    </div>
                </div>
            </section>
            <section class="section bg-default para revealer" id="custom">
                <div class="container">
                    <div class="row row-grid">
                        <div class="col-md-12">
                            <h2 class="display-3 text-center text-white revealer para">Custom-Line Development</h2>
                            <div id="carousel_example1" class="carousel slide" data-ride="carousel" style="padding-top: 5%">
                                <ol class="carousel-indicators">
                                  <?php
                                  $project= \App\Project::where('status', '=', 'Active')->orderBy('id', 'desc')
                                  ->get();
                                  $dataSlide=0;
                                  $class='class="active"';
                                  ?>

                                  @if($project->isEmpty())
                                    @foreach($project as $infoProject)
                                    <li data-target="#carousel_example1" data-slide-to="{{ $dataSlide }}"  @if($dataSlide=='0') {{ $class }} @endif ></li>
                                    <?php $dataSlide++; ?>
                                    @endforeach
                                    @endif
                                </ol>
                                <div class="carousel-inner" style="padding-bottom: 5%">
                                  <?php
                                  $project= \App\Project::where('status', '=', 'Active')->orderBy('id', 'desc')
                                  ->get();
                                  $x=1;
                                  $class="active";
                                  ?>
                                  @foreach($project as $infoProject)
                                  <?php
                                  $projectCommentUrl = "/project-comments/"."/".$infoProject->projectOwnerComment;
                                  $comment = Storage::get($projectCommentUrl);
                                  ?>
                                  <?php $dataToggle = '#projectToggle'.$x; ?>
                                    <div class="carousel-item @if($x=='1') {{ $class }} @endif ?>">
                                        <div class="row" data-toggle="modal" data-target="{{$dataToggle}}">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4 mr-0 text-center">
                                                <img src="{{asset('project/images/').'/'.$infoProject->projectImage}}" alt="Raised circle image" class="img-fluid rounded-circle shadow-lg" style="width: 150px;">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card-body">
                                                    <p class="mb-0 text-white">{{ $infoProject->title }} &nbsp;<small>({{ $infoProject->projectOwner }})</small></p>
                                                    <cite class="text-muted justify-content-center mt-0">
                                                      {!!html_entity_decode($comment)!!}
                                                    </cite>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $x++;?>
                                  @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carousel_example1" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel_example1" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php
            $k=1;
            ?>
            @foreach($project as $value)
            <?php
            $projectid = 'projectToggle'.$k;
            ?>
            <div class="modal fade" id="{{$projectid}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" style="max-width:60%" role="document">
                <div class="modal-content">
                  <?php
                  $descriptionUrl = "/project/"."/".$value->description;
                  $description = Storage::get($descriptionUrl);
                  ?>
                  {!!html_entity_decode($description)!!}

                </div>
              </div>
            </div>
            <?php $k++; ?>
            @endforeach
            <section class="section" id="story">
                <div class="container">
                    <div class="row row-grid">
                        <div class="col-md-12">
                            <h2 class="display-3 text-center revealer para">Stories</h2>
                        </div>
                        <?php
                        $story= \App\Story::where([
                                              ['status', '=', 'Active'],
                                              ['visibleFront', '=', '1'],
                                              ])
                                              ->orderBy('publishDate', 'desc')
                                              ->get();
                        $x=1;
                        ?>
                        @foreach($story as $infoStory)
                        @if($x % 2 == 0)
                        <div class="col-md-12">
                          <div class="row anim">
                              <div class="col-md-2"></div>
                              <div class="col-md-4 text-right" style="padding-top: 2%">
                                  <h5 class="mb-0"><b>{{ $infoStory->publishTitle }}</b></h5>
                                  <ul class="list-unstyled">
                                      <li><small>{{ $infoStory->publishDate }}</small></li>
                                  </ul>
                              </div>
                              <div class="col-md-4 text-left" style="border-left: 2px solid black;">
                                <?php $dataToggle = '#storyToggle'.$x; ?>
                                  <img data-toggle="modal" data-target="{{$dataToggle}}" src="{{asset('story/images/').'/'.$infoStory->publishImage}}" alt="Raised circle image" class="img-fluid rounded shadow-lg">
                              </div>
                          </div>
                        </div>
                              @else
                                <div class="col-md-12 animLeft">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4 text-right">
                                          <?php $dataToggle = '#storyToggle'.$x; ?>
                                            <img data-toggle="modal" data-target="{{$dataToggle}}" src="{{asset('story/images/').'/'.$infoStory->publishImage}}" alt="Raised circle image" class="img-fluid rounded shadow-lg">
                                        </div>
                                        <div class="col-md-4 text-left" style="border-left: 2px solid black; padding-top: 2%">

                                            <h5 class="mb-0"><b>{{ $infoStory->publishTitle }}</b></h5>
                                            <ul class="list-unstyled">
                                                <li><small>{{ $infoStory->publishDate }}</small></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                              @endif
                        <?php $x++; ?>
                        @endforeach
                        <div class="col-md-12 text-center">
                          <a href="/viewAllStories" type="link" class="btn btn-default mt-4">View More Stories</a>
                        </div>
                    </div>
                </div>
            </section>
            <?php
            $k=1;
            ?>
            @foreach($story as $value)
            <?php
            $storyid = 'storyToggle'.$k;
            ?>
            <div class="modal fade" id="{{$storyid}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" style="max-width:60%" role="document">
                <div class="modal-content">
                  <?php
                  $descriptionUrl = "/story/"."/".$value->descriptionPath;
                  $description = Storage::get($descriptionUrl);
                  ?>
                  {!!html_entity_decode($description)!!}

                </div>
              </div>
            </div>
            <?php $k++; ?>
            @endforeach
            <section class="section revealer para bg-success" id="product">
                <div class="container">
                    <div class="row row-grid">
                        <div class="col-md-12 text-center">
                            <h2 class="display-3 text-center">Products</h2>
                            <div class="row">
                              <?php
                              $product= \App\Product::where('status', '=', 'Active')
                                                    ->orderBy('id', 'desc')
                                                    ->get();
                                                    $i = 0;
                              ?>
                              @foreach($product as $infoProduct)
                                <div class="col-md-3">
                                  <?php
                                  $dataToggle = '#exampleModal'.$i;
                                  ?>
                                  <img data-toggle="modal" data-target="{{$dataToggle}}" src="{{asset('product/images/').'/'.$infoProduct->productImage}}" alt="Raised circle image" class="img img-responsive">
                                  <?php $i++; ?>
                                </div>
                              @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <?php
            $product= \App\Product::where('status', 'Active')->orderBy('id', 'desc')->get();
            $j=0;
            ?>
            @foreach($product as $value)
            <?php
            $id = 'exampleModal'.$j;
            ?>
            <div class="modal fade" id="{{$id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" style="max-width:60%" role="document">
                <div class="modal-content">
                  <?php
                  $descriptionUrl = "/product/"."/".$value->description;
                  $description = Storage::get($descriptionUrl);
                  ?>
                  {!!html_entity_decode($description)!!}
                </div>
              </div>
            </div>
            <?php $j++; ?>
            @endforeach
            <section class="section section-lg bg-default" id="career">
                <div class="container">
                    <div class="row row-grid">
                        <div class="col-lg-12 text-center text-white">
                            <h2 class="display-3 text-white revealer para">Current Opening</h2><br>
                            <table class="table table-hover table-striped anim">
                                <thead>
                                    <tr>
                                        <th>Job Title</th>
                                        <th>Area</th>
                                        <th>Last Date</th>
                                        <th>Control Section</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $jobs = \App\Career::where('status', 'Active')->where('isVisible', '1')->orderBy('id', 'desc')->get();
                                  ?>
                                  <?php foreach ($jobs as $key => $value): ?>
                                    <tr>
                                        <td>{{$value->title}}</td>
                                        <td>{{$value->area}}</td>
                                        <td>{{$value->dueDate}}</td>
                                        <td><a class="btn btn-sm btn-1 btn-success" type="link" href="/viewJob/<?php echo $value->id ?>">View</a></td>
                                    </tr>
                                  <?php endforeach; ?>

                                </tbody>
                            </table>
                            <a class="btn btn-success btn-1 para revealer" type="link" href="/viewAllJobs">View All Openings</a>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section section-lg" id="contact">
                <div class="container">
                    <div class="row row-grid">
                        <div class="col-lg-6">
                            <div class="card bg-secondary border-0">
                                <div class="card-header bg-white">
                                    <h2 class="display-3 text-center revealer para">Contact Us</h2>
                                </div>
                                <div class="card-body para revealer">
                                      <form action="/queries" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-window-maximize"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Subject" type="text" name="subject" required>
                                        </div>
                                        <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Contact" type="text" name="contact" required>
                                        </div>
                                        <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Email" type="email" name="email" required>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" id="editor" rows="3" placeholder="Write your message here ..." name="querydescription"></textarea>
                                            <script>
                                              ClassicEditor
                                                  .create( document.querySelector( '#editor' ) )
                                                  .catch( error => {
                                                      console.error( error );
                                                  } );
                                            </script>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-success">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card bg-secondary border-0" style="height: 100%">
                                <div class="card-header bg-white">
                                    <h2 class="display-3 text-center revealer para">Our Location</h2>
                                </div>
                                <?php
                                $ourInfo= \App\Ourinformation::first();
                                ?>
                                  @if(!empty($ourInfo->location))
                                <?php
                                $mystring = $ourInfo->location;
                                $findme   = 'iframe';
                                $pos = strpos($mystring, $findme);
                                if ($pos === false) {
                                  echo "Not Supportable";
                                }
                                else {
                                  echo $ourInfo->location;
                                }
                                ?>


                        @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
@endsection

@if(!empty($message) and !empty($color))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
"<?php echo $color; ?>"
@endsection
@endif
