@extends('layouts.app')
@section('content')
<div class="card" class="col-md-12">
  <?php
  $project = \App\Project::find($id);
  ?>
  <img class="card-img-top" src="{{asset('project/images/').'/'.$project->projectImage}}" height="500em" alt="Card image cap">
  <div class="card-body">
    <h2 class="card-title">{{$project->title}} <small>({{$project->status}})</small></h2>
    <p><a href="{{$project->URL}}" target="_new">Visit</a></p>

    <div class="row">
      <div class="col-md-6">
        <p class="card-text"><b>Uploaded By:</b></p>
        <p class="card-text"><b>Project Owner</b></p>
        <p class="card-text"><b>Collaboration Date:</b></p>
        <p class="card-text"><b>Owner Comments:</b></p>

      </div>
      <div class="col-md-6">
        <?php $uploader = \App\User::find($project->uploaderID); ?>
        <p class="card-text">{{$uploader->name}}</p>
        <p class="card-text">{{$project->projectOwner}}</p>
        <p class="card-text">{{$project->collaborationDate}}</p>
      </div>
      <div class="col-md-12">
        <?php
        $commentUrl = "/project-comments/"."/".$project->projectOwnerComment;
        $comment = Storage::get($commentUrl);
        ?>
        {!!html_entity_decode($comment)!!}
      </div>

      <div class="col-md-12">
        <p class="card-text"><b>Description:</b></p>
        <?php
        $descriptionUrl = "/project/"."/".$project->description;
        $description = Storage::get($descriptionUrl);
        ?>
        {!!html_entity_decode($description)!!}
      </div>
    </div>
  </div>
</div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
