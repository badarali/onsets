@extends('layouts.app')

@section('content')
<div class="row">
  <?php
  $project = \App\Project::find($id);
  ?>
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            @if(!empty($project->projectImage))
            <img class="card-img-top" src="{{asset('project/images/').'/'.$project->projectImage}}" height="500em" alt="Card image cap">
            @endif
            <?php
            $media = \App\Media::where('for', 'project')->where('forId', $project->id)->get();
            ?>
            <div class="card-body">
              @if(!$media->isEmpty())
              <div class="row text-center mt-4">
                @foreach($media as $value)
                @if($value->type == 'image')
                <div class="col-md-3">
                  <img class="img img-responsive col-md-12" src="{{asset('project/images/').'/'.$value->name}}">
                  <small class="col-md-12 text-center">{{$value->name}}</small>
                  <form action="{{ route('media.destroy', $value->id) }}" method="post" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input hidden value="{{$project->id}}" name="forId">
                    <input hidden value="project" name="type">
                    <input type="submit" class="btn btn-danger btn-sm mt-2 mb-2" placeholder="Delete" value="Delete"/>
                  </form>
                </div>
                @elseif($value->type == 'video')
                <div class="col-md-3">
                  <video class="video col-md-12 mb-4" controls>
                    <source src="{{asset('project/videos/').'/'.$value->name}}">
                  </video>
                  <small class="col-md-12 text-center">{{$value->name}}</small>
                  <form action="{{ route('media.destroy', $value->id, $project->id, 'project') }}" method="post" style="display:inline">
                    <input hidden value="{{$project->id}}" name="forId">
                    <input hidden value="project" name="type">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger btn-sm mt-2 mb-2" placeholder="Delete" value="Delete"/>
                  </form>
                </div>
                @endif
                @endforeach
              </div>
              @endif
              <form action="/projects/<?php echo $project->id ?>" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT')}}
                <input type="hidden" value="{{ $project->id}}" name="id">
                <div>
                  <div class="row">
                      <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Title</label>
                        <input type="text" required id="input-postal-code" name="name" class="form-control form-control-alternative" placeholder="Title"  value="{{ $project->title}}" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Project Owner</label>
                        <input type="text" required id="input-postal-code" name="projectOwner" class="form-control form-control-alternative" placeholder="Project Owner"  value="{{ $project->projectOwner}}" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Collaboration Date</label>
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                            </div>
                            <input value="{{$project->collaborationDate}}" class="form-control datepicker" name="collabrationDate" placeholder="Select date" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                          <div class="form-group">
                        <label class="form-control-label" for="input-country">Link</label>
                        <input type="url" required id="input-postal-code" name="link" class="form-control form-control-alternative" placeholder="Link"  value="{{ $project->URL}}" >
                      </div>
                    </div>
                         <div class="col-lg-6">
                           <div class="form-group">
                             <label class="form-control-label" for="input-country">Status</label>
                             <select required name="status" class="form-control form-control-alternative">
                               @if($project->status=='Active')
                               <option value="Active" selected>Active</option>
                               <option value="Deactive">Deactive</option>
                               @elseif($project->status=='Deactive')
                               <option value="Active">Active</option>
                               <option value="Deactive" selected>Deactive</option>
                               @endif
                             </select>
                           </div>
                         </div>
                         <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-control-label" for="input-email">Primary Image <small>(Only PNG, JPG, JPEG formats allowed)</small></label>
                              <label class="btn btn-success" for="image"><i class="fas fa-upload"></i> Upload Image</label><small id="imageLabel"></small>
                              <input type="file" id="image" style="position:absolute; opacity:0; z-index:-1" name="image" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png">
                             </div>
                         </div>
                         <div class="col-lg-6">
                           <div class="form-group">
                             <label class="form-control-label" for="input-email">Images <small>(Only PNG, JPG, JPEG formats allowed)</small></label><br>
                             <label class="btn btn-success" for="images"><i class="fas fa-upload"></i> Upload Images</label><small id="imagesLabel"></small>
                             <input type="file" id="images" style="position:absolute; opacity:0; z-index:-1" name="images[]" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png" multiple>
                             </div>
                         </div>
                         <div class="col-lg-6">
                           <div class="form-group">
                             <label class="form-control-label" for="input-email">Videos <small>(Only FLV, MP4, M4P, MPEG, MOV formats allowed)</small></label>
                             <label class="btn btn-success" for="videos"><i class="fas fa-upload"></i> Upload Videos</label><small id="videosLabel"></small>
                             <input type="file" id="videos" style="position:absolute; opacity:0; z-index:-1" name="videos[]" class="form-control form-control-alternative" accept=".mp4,.m4p,.mpeg,.webm,.mov,.mpg,.flv" multiple>
                             </div>
                         </div>
                         <div class="col-md-12">
                           <div class="form-group">
                             <?php
                             $descriptionUrl = "/project/"."/".$project->description;
                             $description = Storage::get($descriptionUrl);
                             ?>

                             <label class="form-control-label" for="input-username">Project Description</label>
                             <textarea class="form-control" rows="5" name="editor1" value="{{$project->description}}" required>{!!html_entity_decode($description)!!}</textarea>
                           </div>
                         </div>
                         <?php
                         $CommentUrl = "/project-comments/"."/".$project->projectOwnerComment;
                         $comment = Storage::get($CommentUrl);
                         ?>
                         <div class="col-md-12">
                           <div class="form-group">
                             <label class="form-control-label" for="input-username">Project Owner Comments</label>
                             <textarea class="form-control" rows="5" name="comments"  value="{{ $project->projectOwnerComment }}" required>{!!html_entity_decode($comment)!!}</textarea>
                           </div>
                         </div>
                    </div>
                </div>
                <!-- Save Button -->
                <div class="row">
                  <div class="col-md-12 text-right">
                    <input class="btn btn-default" type="submit" value="Update"></input>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
