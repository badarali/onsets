@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <div class="col-8">
            <h3 class="mb-0">Project</h3>
          </div>
        </div>
      </div>
      <div class="card-body">
        <form action="/projects" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div>
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-country">Title</label>
                  <input required type="text" id="input-postal-code" name="name" class="form-control form-control-alternative" placeholder="Title">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                             <label class="form-control-label" for="input-country">Project Owner</label>
                             <input required  type="text" id="input-postal-code" name="projectOwner" class="form-control form-control-alternative" placeholder="Project Owner">
                           </div>
                         </div>
                         <div class="col-md-6">
                           <div class="form-group">
                             <label class="form-control-label" for="input-country">Collaboration Date</label>
                             <div class="input-group input-group-alternative">
                                 <div class="input-group-prepend">
                                     <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                 </div>
                                 <input class="form-control datepicker" name="collabrationDate" placeholder="Select date" type="text" required>
                             </div>
                           </div>
                         </div>
                         <div class="col-md-6">
                               <div class="form-group">
                             <label class="form-control-label" for="input-country">Link</label>
                             <input required type="url" id="input-postal-code" name="link" class="form-control form-control-alternative" placeholder="Link">
                           </div>
                         </div>

                         <div class="col-md-6">
                           <div class="form-group">
                             <label class="form-control-label" for="input-country">Status</label>
                             <select required name="status" class="form-control form-control-alternative">
                               <option value="Active">Active</option>
                               <option value="Deactive">Deactive</option>
                             </select>
                           </div>
                         </div>
                         <div class="col-lg-6">
                           <div class="form-group">
                             <label class="form-control-label">Primary Image <small>(Only PNG, JPG, JPEG formats allowed)</small></label>
                             <label class="btn btn-success" for="image"><i class="fas fa-upload"></i> Upload Image</label><small id="imageLabel"></small>
                             <input required id="image" type="file" style="position:absolute; opacity:0; z-index:-1" name="image" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png">
                          </div>
                         </div>
                         <div class="col-lg-6">
                           <div class="form-group">
                             <label class="form-control-label">Images <small>(Only PNG, JPG, JPEG formats allowed)</small></label><br>
                             <label class="btn btn-success" for="images"><i class="fas fa-upload"></i> Upload Images</label><small id="imagesLabel"></small>
                             <input type="file" id="images" style="position:absolute; opacity:0; z-index:-1" name="images[]" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png" multiple>
                           </div>
                         </div>
                         <div class="col-lg-6">
                           <div class="form-group">
                             <label class="form-control-label">Videos <small>(Only FLV, MP4, M4P, MPEG, MOV formats allowed)</small></label>
                             <label class="btn btn-success" for="videos"><i class="fas fa-upload"></i> Upload Videos</label><small id="videosLabel"></small>
                             <input type="file" id="videos" style="position:absolute; opacity:0; z-index:-1" name="videos[]" class="form-control form-control-alternative" accept=".mp4,.m4p,.mpeg,.webm,.mov,.mpg,.flv" multiple>
                           </div>
                         </div>
                         <div class="col-md-12">
                           <div class="form-group">
                             <label class="form-control-label" for="input-username">Project Owner Comments</label>
                             <textarea class="form-control" rows="5" required name="comments" placeholder="Project Owner Comments"></textarea>
                           </div>
                         </div>
                         <div class="col-md-12">
                           <div class="form-group">
                             <label class="form-control-label" for="input-username">Project Description</label>
                             <textarea name="editor1" required class="form-control" rows="5" placeholder="Project Description"></textarea>
                           </div>
                         </div>
                    </div>
                </div>
                <!-- Save Button -->
                <div class="row">
                  <div class="col-md-12 text-right">
                    <input class="btn btn-default" type="submit" value="Save"></input>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
