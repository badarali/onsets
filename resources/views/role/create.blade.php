@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
        </div>
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Role</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form action="/roles" method="post">
                {{ csrf_field() }}
                <div class="pl-lg-4">
                  <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Authority</label>
                        <input required type="text" id="input-postal-code" name="authority" class="form-control form-control-alternative" placeholder="Authority">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Status</label>
                        <select required name="status" class="form-control form-control-alternative">
                          <option value="Active">Active</option>
                          <option value="Deactive">Deactive</option>
                        </select>
                      </div>
                    </div>

                    </div>
                </div>

                <!-- Save Button -->
                  <div class="col-12 text-right">
                <input class="btn btn-default" type="submit" value="Save"></input>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
