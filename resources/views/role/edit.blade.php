@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Role</h3>
                </div>
              </div>
            </div>

            <div class="card-body">
              <form action="/roles/<?php echo $role->id;?>" method="post" >
                {{ csrf_field() }}
                {{method_field('PUT')}}
                  <input type="hidden" value="{{ $role->id}}" name="id">
                <div class="pl-lg-4">
                  <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Title</label>
                        <input type="text" required id="input-postal-code"  name="authority" class="form-control form-control-alternative" placeholder="Authority"  value="{{ $role->authority }}">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Status</label>
                        <select required name="status" class="form-control form-control-alternative">
                          @if($role->status=='Active')
                          <option value="Active" selected>Active</option>
                          <option value="Deactive">Deactive</option>
                          @elseif($role->status=='Deactive')
                          <option value="Active">Active</option>
                          <option value="Deactive" selected>Deactive</option>
                          @endif
                        </select>
                      </div>
                    </div>


                    </div>
                </div>
                <!-- Save Button -->
                <div class="col-md-12 text-right">
                  <input class="btn btn-default" type="submit" value="Update"></input>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
