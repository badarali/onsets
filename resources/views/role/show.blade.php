@extends('layouts.app')

@section('content')
<div class="card" class="col-md-12">
  <div class="card-body">
    <h2 class="card-title">{{$role->authority}} <small>({{$role->status }})</small>
      <?php
      $uploader = \App\User::find($role->uploaderID);
      ?>
      <div style="display:inline; float: right">
      <small><b>Created By:</b> {{ $uploader->name }}</small></div>
    </h2>
  </div>
</div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
