@extends('layouts.app')
@section('content')
<div class="card" class="col-md-12">
  <?php
  $slider = \App\Slider::find($id);
  ?>
  <div class="card-header border-0 bg-secondary">
    <h3 class="mb-0">Slider</h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-md-6">
        <p><b>Status</b></p>
        <p><b>Uploaded By</b></p>
      </div>
      <div class="col-md-6">
        <p class="card-text">
          {{$slider->status}}
          <?php
          $uploader = \App\User::find($slider->uploaderID);
          ?>
        </p>
        <p>{{$uploader->name}}</p>
      </div>
    </div>
    <p class="card-text" id="desc">
      <b>Description:</b><br>
    <?php
    $contentUrl = "/slider/"."/".$slider->content;
    $content = Storage::get($contentUrl);
    ?>
    {!!html_entity_decode($content)!!}
    </p>
  </div>
</div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
