@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Slider</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form action="/sliders" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Primary Image <small>(Only PNG, JPG, JPEG formats allowed)</small></label>
                        <label class="btn btn-success" for="image"><i class="fas fa-upload"></i> Upload Image</label><small id="imageLabel"></small>
                        <input type="file" id="image" style="position:absolute; opacity:0; z-index:-1" name="file" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png, .flv, .mp4, .mov, .wemb, .mpeg">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Status</label>
                        <select required name="status" class="form-control form-control-alternative">
                          <option value="Active">Active</option>
                          <option value="Deactive">Deactive</option>
                        </select>
                      </div>
                    </div>
                       <div class="col-md-12">
                           <div class="form-group">
                             <label class="form-control-label" for="input-username">Content</label>
                             <textarea class="form-control" rows="5" name="content" placeholder="Content"></textarea>
                           </div>
                         </div>
                    </div>
                </div>
                <!-- Save Button -->
                <div class="row">
                  <div class="col-12 text-right">
                    <input class="btn btn-default" type="submit" value="Save"></input>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
