@extends('layouts.app')
@section('content')
<div class="row">
  <?php $slider = \App\Slider::find($id); ?>
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Slider</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <?php
              $media = \App\Media::where('for', 'slider')->where('forId', $slider->id)->get();
              ?>
              @if(!$media->isEmpty())
              <div class="row text-center mt-4">
                @foreach($media as $value)
                @if($value->type == 'image')
                <div class="col-md-3">
                  <img class="img img-responsive col-md-12" src="{{asset('slider/images/').'/'.$value->name}}">
                  <small class="col-md-12 text-center">{{$value->name}}</small>
                  <form action="{{ route('media.destroy', $value->id) }}" method="post" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input hidden value="{{$slider->id}}" name="forId">
                    <input hidden value="slider" name="type">
                    <input type="submit" class="btn btn-danger btn-sm mt-2 mb-2" placeholder="Delete" value="Delete"/>
                  </form>
                </div>
                @elseif($value->type == 'video')
                <div class="col-md-3">
                  <video class="video col-md-12 mb-4" controls>
                    <source src="{{asset('slider/videos/').'/'.$value->name}}">
                  </video>
                  <small class="col-md-12 text-center">{{$value->name}}</small>
                  <form action="{{ route('media.destroy', $value->id, $slider->id, 'project') }}" method="post" style="display:inline">
                    <input hidden value="{{$slider->id}}" name="forId">
                    <input hidden value="slider" name="type">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger btn-sm mt-2 mb-2" placeholder="Delete" value="Delete"/>
                  </form>
                </div>
                @endif
                @endforeach
              </div>
              @endif
              <form action="/sliders/<?php echo $slider->id ?>" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Primary Image <small>(Only PNG, JPG, JPEG formats allowed)</small></label>
                        <label class="btn btn-success" for="image"><i class="fas fa-upload"></i> Upload Image</label><small id="imageLabel"></small>
                        <input type="file" id="image" style="position:absolute; opacity:0; z-index:-1" name="file" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png, .flv, .mp4, .mov, .wemb, .mpeg">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Status</label>
                        <select required name="status" class="form-control form-control-alternative">
                          @if($slider->status=='Active')
                          <option value="Active" selected>Active</option>
                          <option value="Deactive">Deactive</option>
                          @elseif($slider->status=='Deactive')
                          <option value="Active">Active</option>
                          <option value="Deactive" selected>Deactive</option>
                          @endif
                        </select>
                      </div>
                    </div>
                      <div class="col-md-12">
                         <div class="form-group">
                           <?php
                           $contentUrl = "/slider/"."/".$slider->content;
                           $content = Storage::get($contentUrl);
                           ?>
                           <label class="form-control-label" for="input-username">Content</label>
                           <textarea class="form-control" rows="5" name="content" placeholder="Content">{!!html_entity_decode($content)!!}</textarea>
                         </div>
                       </div>
                    </div>
                </div>
                <!-- Save Button -->
                <div class="row">
                  <div class="col-12 text-right">
                    <input class="btn btn-default" type="submit" value="Save"></input>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
