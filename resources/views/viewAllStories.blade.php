@extends('layouts.website')
@section('content')
<section class="section" id="story">
    <div class="container">
        <div class="row row-grid">
            <div class="col-md-12"><br>
                <h2 class="display-3 text-center revealer para">Stories</h2>
            </div>
            <?php
            $story= \App\Story::where([['status', '=', 'Active']])->orderBy('publishDate', 'asc')->get();
            $x=1;
            ?>
            @foreach($story as $infoStory)
            @if($x % 2 == 0)
            <div class="col-md-12">
              <div class="row anim">
                  <div class="col-md-2"></div>
                  <div class="col-md-4 text-right" style="padding-top: 2%">
                      <h5 class="mb-0"><b>{{ $infoStory->publishTitle }}</b></h5>
                      <ul class="list-unstyled">
                          <li><small>{{ $infoStory->publishDate }}</small></li>
                      </ul>
                  </div>
                  <div class="col-md-4 text-left" style="border-left: 2px solid black;">
                    <?php $dataToggle = '#storyToggle'.$x; ?>
                      <img data-toggle="modal" data-target="{{$dataToggle}}" src="{{asset('story/images/').'/'.$infoStory->publishImage}}" alt="Raised circle image" class="img-fluid rounded shadow-lg">
                  </div>
              </div>
            </div>
                  @else
                    <div class="col-md-12 animLeft">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-4 text-right">
                              <?php $dataToggle = '#storyToggle'.$x; ?>
                                <img data-toggle="modal" data-target="{{$dataToggle}}" src="{{asset('story/images/').'/'.$infoStory->publishImage}}" alt="Raised circle image" class="img-fluid rounded shadow-lg">
                            </div>
                            <div class="col-md-4 text-left" style="border-left: 2px solid black; padding-top: 2%">

                                <h5 class="mb-0"><b>{{ $infoStory->publishTitle }}</b></h5>
                                <ul class="list-unstyled">
                                    <li><small>{{ $infoStory->publishDate }}</small></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                  @endif
            <?php $x++; ?>
            @endforeach
        </div>
    </div>
</section>
<?php
$k=1;
?>
@foreach($story as $value)
<?php
$storyid = 'storyToggle'.$k;
?>
<div class="modal fade" id="{{$storyid}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" style="max-width:60%" role="document">
    <div class="modal-content">
      <?php
      $descriptionUrl = "/story/"."/".$value->descriptionPath;
      $description = Storage::get($descriptionUrl);
      ?>
      {!!html_entity_decode($description)!!}

    </div>
  </div>
</div>
<?php $k++; ?>
@endforeach
@endsection
