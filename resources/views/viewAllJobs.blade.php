@extends('layouts.website')
@section('content')
<section class="section section-shaped section-lg">
      <div class="shape shape-style-1 bg-gradient-gray">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <?php
        $jobs = \App\Career::where('status', 'Active')->get();
        ?>
      </div>
      <div class="container pt-lg-md">
        <div class="row row-grid">
            <div class="col-lg-12">
              <div class="card bg-secondary shadow border-0">
                <div class="card-header bg-white">
                  <div class="text-muted text-center">
                    <h3 class="heading"><b>Current</b> Opening<h3>
                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-hover table-striped anim">
                      <thead>
                          <tr>
                              <th>Job Title</th>
                              <th>Area</th>
                              <th>Due Date</th>
                              <th class="text-center">Positions</th>
                              <th>Control Section</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php
                        $jobs = \App\Career::where('status', 'Active')->where('isVisible', '1')->get();
                        ?>
                        <?php foreach ($jobs as $key => $value): ?>
                          <tr>
                              <td>{{$value->title}}</td>
                              <td>{{$value->area}}</td>
                              <td>{{$value->dueDate}}</td>
                              <td class="text-center">{{$value->noOfPosition}}</td>
                              <td><a class="btn btn-sm btn-1 btn-success" type="link" href="/viewJob/<?php echo $value->id ?>">View</a></td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
@endsection
