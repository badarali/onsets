@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">User
                <div style="display: inline; float: right" >
                  <a type="link" class="btn btn-sm btn-primary pull-right" href="{{URL::asset('users/create')}}">Add User</a>
                </div>
              </h3>
            </div>

            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Role</th>
                    <th scope="col">Status</th>
                    <th scope="col">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $info)
                  <tr>
                    <td>{{$info->id}}</td>
                    <th scope="row">

                          <span class="mb-0 text-sm align-items-center">{{$info->name}}</span>

                    </th>
                    <td>
                      <span class="mb-0 text-sm align-items-center">{{$info->email}}</span>
                    </td>
                    <td>
                      <?php
                      $role = \App\Role::find($info->RoleID);
                      ?>
                      <span class="mb-0 text-sm align-items-center">{{ $role->authority }}</span>
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                        @if($info->status == 'Deactive')
                        <i class="bg-warning"></i> Deactive
                        @elseif($info->status == 'Active')
                        <i class="bg-success"></i> Active
                        @endif
                      </span>
                    </td>

                    <td>
                        <a type="link" class="btn btn-success btn-sm" href="/users/<?php echo $info->id; ?>">View</a>
                        <a type="link" class="btn btn-default btn-sm" href="/users/<?php echo $info->id;?>/edit">Edit</a>
                        <form action="{{ route('users.destroy', $info->id) }}" method="post" style="display:inline">
                                  {{ method_field('DELETE') }}
                                  {{ csrf_field() }}
                          <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                        </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'<?php echo $color; ?>'
@endsection
@endif
