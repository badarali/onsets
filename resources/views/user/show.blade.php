@extends('layouts.app')
@section('content')
<div class="row">

        <div class="col-md-12">
          <div class="card">
            <div class="card-body">

              <?php
              $role = \App\Role::find($data->RoleID);
              ?>
              <h2 class="card-title">{{$data->name}} <small>({{   $role->authority }})</small></h2>
              <div class="row">
                <div class="col-md-4">
                  <p class="card-text"><b>Name:</b></p>
                </div>
                <div class="col-md-8">
                  <p class="card-text">{{$data->name}}</p>
                </div>

                <div class="col-md-4">
                  <p class="card-text"><b>Email:</b></p>
                </div>
                <div class="col-md-8">
                  <p class="card-text">{{$data->email}}</p>
                </div>
                <div class="col-md-4">
                  <p class="card-text"><b>Role:</b></p>
                </div>

                <div class="col-md-8">
                  <p class="card-text">{{ $role->authority}}</p>
                </div>
                <div class="col-md-4">
                  <p class="card-text"><b>Status:</b></p>
                </div>
                <div class="col-md-8">
                  <p class="card-text">{{$data->status}}</p>
                </div>


                </div>

              </div>
            </div>


          </div>
        </div>


<br>
      <div class="row">
              <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
              </div>
              <div class="col-md-12">
                <div class="card bg-secondary shadow">
                  <div class="card-header bg-white border-0">
                    <div class="row align-items-center" >
                      <div class="col-md-10">
                        <h3 class="mb-0">Contact Section</h3>
                      </div>
                      <div style="display: inline; float: right;" class="col-md-2" >
                        <a type="link" class="btn btn-sm btn-primary pull-right" href="{{ '/userContact/'.$data->id}}">Add Contact</a>
                      </div>
                    </div>


                  </div>

                  <div class="card-body">

                    <div class="table-responsive">
                      <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Contact Type</th>
                            <th scope="col">Conatct</th>
                            <th scope="col">Control Section</th>
                          </tr>
                        </thead>
                        <?php
                        $contact = \App\Contact::where('user_id', '=', $data->id)->orderBy('id', 'desc')
                        ->get();
                        ?>
                        <tbody>
                        @foreach($contact as $info)
                          <tr>
                            <td>{{$info->id}}</td>
                            <th scope="row">

                                  <span class="mb-0 text-sm align-items-center">{{   $info->contactOF }}</span>

                            </th>
                            <td>
                              <span class="mb-0 text-sm align-items-center">{{$info->contactNumber}}</span>
                            </td>


                            <td>

                                <a type="link" class="btn btn-default btn-sm" href="/ContactEdit/{{$data->id}}/{{ $info->id}}">Edit</a>
                                <form action="/usersContactD/{{$data->id}}/{{ $info->id}}" method="post" style="display:inline">
                                          {{ csrf_field() }}
                                  <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                                </form>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>


                  </div>
                </div>
              </div>
            </div>
<br>
<div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
        </div>
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-md-10">
                  <h3 class="mb-0">Address section</h3>
                </div>
                <div style="display: inline; float: right;" class="col-md-2" >
                  <a type="link" class="btn btn-sm btn-primary pull-right" href="{{ '/userAddress/'.$data->id}}">Add Address</a>
                </div>
              </div>
            </div>

            <div class="card-body">

<br>
              <div class="table-responsive">
                <table class="table align-items-center table-flush">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Address Type</th>
                      <th scope="col">Addresss</th>
                      <th scope="col">Control Section</th>
                    </tr>
                  </thead>
                  <?php
                  $address = \App\Address::where('user_id', '=', $data->id)->orderBy('id', 'desc')
                  ->get();
                  ?>
                  @foreach($address as $info)
                  <tbody>
                    <tr>
                      <td>{{$info->id}}</td>
                      <th scope="row">

                            <span class="mb-0 text-sm align-items-center">{{ $info->addressOF  }}</span>

                      </th>
                      <td>
                        <span class="mb-0 text-sm align-items-center">{{$info->addressBody}}</span>
                      </td>


                      <td>

                          <a type="link" class="btn btn-default btn-sm" href="/AddressEdit/{{$data->id}}/{{ $info->id}}">Edit</a>
                          <form action="/usersAddressD/{{$data->id}}/{{ $info->id}}" method="post" style="display:inline">
                                    {{ csrf_field() }}
                            <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                          </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>


            </div>
          </div>
        </div>
      </div>


@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'<?php echo $color; ?>'
@endsection
@endif
