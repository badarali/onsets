@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
        </div>
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">User</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form method="POST" action="/users">
                  {{ csrf_field() }}
                <div class="pl-lg-4">
                  <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Name</label>
                        <input required type="text" id="input-postal-code" name="name" class="form-control form-control-alternative" placeholder="Name">
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-country">Email</label>
                      <input required type="email" id="input-postal-code" name="email" class="form-control form-control-alternative" placeholder="Email">
                    </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-country">Password</label>
                    <input required type="password" id="input-postal-code" name="password" class="form-control form-control-alternative" placeholder="Password">
                  </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                    <label class="form-control-label" for="input-country">Role</label>
                  <select required name="role" class="form-control form-control-alternative">

                    <?php
                    $roles = \App\Role::where('status', '=', 'Active')->get();
                    ?>
                    @foreach($roles as $info)
                    <option value="{{ $info->id}}">{{ $info->authority}}</option>
                    @endforeach

                  </select>

                </div>
              </div>

              <div class="col-md-6">
              <div class="form-group">
                  <label class="form-control-label" for="input-country">Conatct Type</label>
                <select required name="contactType" class="form-control form-control-alternative">
                  <option value="Personal">Personal</option>
                  <option value="Home">Home</option>
                  <option value="Office">Office</option>
                  <option value="Other">Other</option>
                </select>

              </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
              <label class="form-control-label" for="input-country">Contact</label>
              <input required type="text" id="input-postal-code" name="Contact" class="form-control form-control-alternative" placeholder="Contact">
            </div>
          </div>

          <div class="col-md-6">
          <div class="form-group">
              <label class="form-control-label" for="input-country">Addres Type</label>
            <select required name="addressType" class="form-control form-control-alternative">
              <option value="Home">Home</option>
              <option value="Office">Office</option>
              <option value="Other">Other</option>
            </select>

          </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
          <label class="form-control-label" for="input-country">Address</label>
          <input required type="text" id="input-postal-code" name="address" class="form-control form-control-alternative" placeholder="Address">
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <label class="form-control-label" for="input-country">Status</label>
          <select required name="status" class="form-control form-control-alternative">
            <option value="Active">Active</option>
            <option value="Deactive">Deactive</option>
          </select>
        </div>
      </div>


                    </div>
                </div>
                <!-- Save Button -->
                  <div class="col-12 text-right">
                <input class="btn btn-default" type="submit" value="Save"></input>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
