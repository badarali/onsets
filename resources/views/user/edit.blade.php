@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
        </div>
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">User</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form method="POST" action="/users/<?php echo $data->id;?>">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div class="pl-lg-4">
                  <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Name</label>
                        <input required type="text" id="input-postal-code" name="name" class="form-control form-control-alternative" placeholder="Name" value="{{ $data->name}}">
                      </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-country">Email</label>
                      <input required type="email" id="input-postal-code" name="email" class="form-control form-control-alternative" placeholder="Email"  value="{{ $data->email}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-country">Password</label>
                    <input  type="password" id="input-postal-code" name="password" class="form-control form-control-alternative" placeholder="Password">
                  </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                    <label class="form-control-label" for="input-country">Role</label>
                  <select required name="role" class="form-control form-control-alternative">

                    <?php
                    $roles = \App\Role::where('status', '=', 'Active')->get();
                    ?>
                    @foreach($roles as $info)
                    <option value="{{ $info->id}}">{{ $info->authority}}</option>
                    @endforeach

                  </select>

                </div>
              </div>


      <div class="col-md-6">
        <div class="form-group">
          <label class="form-control-label" for="input-country">Status</label>
          <select required name="status" class="form-control form-control-alternative">
            @if($data->status=='Active')
            <option value="Active" selected>Active</option>
            <option value="Deactive">Deactive</option>
            @elseif($data->status=='Deactive')
            <option value="Active">Active</option>
            <option value="Deactive" selected>Deactive</option>
            @endif

          </select>
        </div>
      </div>


                    </div>
                </div>
                <!-- Save Button -->
                  <div class="col-12 text-right">
                <input class="btn btn-default" type="submit" value="Update"></input>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
