@extends('layouts.app')
@section('content')
<div class="row">
  <?php
  $applicant = \App\Applicant::find($id);
  ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <h2 class="card-title">{{$applicant->name}} <small>({{$applicant->status}})</small></h2>
              <div class="row">
                <div class="col-md-6">
                  <p class="card-text"><b>Email:</b></p>
                  <p class="card-text"><b>Contact:</b></p>
                  <p class="card-text"><b>Resume:</b></p>
                  @if(!empty($applicant->linkedInProfile))
                  <p class="card-text"><b>LinkedIn Profile:</b></p>
                  @endif
                  <p class="card-text"><b>Address:</b></p>
                  <p class="card-text"><b>Apply Date:</b></p>
                  <p class="card-text"><b>Cover Letter:</b></p>
                </div>
                <div class="col-md-6">
                  <p class="card-text">{{$applicant->email}}</p>
                  <p class="card-text">{{$applicant->contactNO}}</p>
                  <?php
                  $url = 'applicant/document'.'/'.$applicant->resume;
                  ?>
                  <p class="card-text"><a href="{{asset('applicant/document/').'/'.$applicant->resume}}" target="_new">Resume</a></p>
                  @if(!empty($applicant->linkedInProfile))
                  <p class="card-text">{{$applicant->linkedInProfile}}</p>
                  @endif
                  <p class="card-text">{{$applicant->address}}</p>
                  <p class="card-text">{{$applicant->created_at}}</p>
                </div>
                <div class="col-md-12">
                  <p class="card-text">
                  <?php
                  $coverLetterUrl = "/applicant/coverLetter/"."/".$applicant->coverLetter;
                  $coverLetter = Storage::get($coverLetterUrl);
                  ?>
                  {!!html_entity_decode($coverLetter)!!}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
