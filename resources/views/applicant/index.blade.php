@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Applicants</h3>
            </div>
            <?php
            $applicants = \App\Applicant::where('careerId', $job_id)->get();
            ?>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Applied On</th>
                    <th scope="col">Status</th>
                    <th scope="col">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($applicants as $info)
                  <tr>
                    <td>{{$info->id}}</td>
                    <th scope="row">
                      <div class="media align-items-center">
                        <div class="media-body">
                          <span class="mb-0 text-sm">{{$info->name}}</span>
                        </div>
                      </div>
                    </th>
                    <td>{{$info->email}}</td>
                    <td>{{$info->created_at}}</td>
                    <td>
                      <span class="badge badge-dot mr-4">
                        @if($info->status == 'Applied')
                        <i class="bg-success"></i> Applied
                        @elseif($info->status == 'Rejected')
                        <i class="bg-warning"></i> Rejected
                        @else
                        <i class="bg-success"></i> {{$info->status}}
                        @endif
                      </span>
                    </td>
                    <td>
                        <a type="link" class="btn btn-success btn-sm" href="/applicants/<?php echo $info->id; ?>">View</a>
                        <a type="link" class="btn btn-default btn-sm" href="/applicants/<?php echo $info->id;?>/edit">Edit</a>
                        <form action="{{ route('applicants.destroy', $info->id) }}" method="post" style="display:inline">
                                  {{ method_field('DELETE') }}
                                  {{ csrf_field() }}
                          <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                        </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
@if(!empty($color))
'<?php echo $color; ?>'
@endif
@endsection
@endif
