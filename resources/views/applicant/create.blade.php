@extends('layouts.website')
@section('content')
<section class="section section-shaped section-lg">
      <div class="shape shape-style-1 bg-gradient-gray">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div class="container pt-lg-md">
        <div class="row justify-content-center">
          <div class="col-lg-9">
            <div class="card bg-secondary shadow border-0">
              <div class="card-header bg-white">
                <div class="text-muted text-center">
                  <h3 class="heading">Apply Here<h3>
                </div>
              </div>
              <div class="card-body py-lg-5">
                <form role="form" action="/applicants" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input value="{{$id}}" name="careerId" hidden>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Full Name</label>
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                          </div>
                          <input class="form-control" placeholder="Full Name" type="text" name="name" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Email</label>
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                          </div>
                          <input class="form-control" placeholder="Email" type="email" name="email" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Contact</label>
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-mobile-alt"></i></span>
                          </div>
                          <input class="form-control" placeholder="Contact" type="number" name="contactNo" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Resume/CV</label>
                        <div class="mb-3">
                          <label class="btn btn-success" for="image"><i class="fas fa-upload"></i> Upload Image</label><small id="imageLabel"></small>
                          <input id="image" style="position:absolute; opacity:0; z-index:-1" class="form-control" type="file" name="document" accept=".pdf" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>LinkedIn Profile</label>
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fab fa-linkedin"></i></span>
                          </div>
                          <input class="form-control" type="url" placeholder="LinkedIn Profile" name="linkedIn">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Address</label>
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                          </div>
                          <input class="form-control" placeholder="Address" type="text" name="address" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Cover Letter</label>
                        <textarea name="coverLetter" id="editor"></textarea>
                        <script>
                          ClassicEditor
                              .create( document.querySelector( '#editor' ) )
                              .catch( error => {
                                  console.error( error );
                              } );
                        </script>
                      </div>
                    </div>
                  </div>
                  <div class="text-right">
                    <input type="submit" class="btn btn-default mt-4" value="Apply">
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection
@if(!empty($messages))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $messages;?>"
@endsection
@section('barcolor')
'<?php echo $color; ?>'
@endsection
@endif
