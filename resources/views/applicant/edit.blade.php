@extends('layouts.app')
@section('content')
<section class="section section-shaped section-lg">
  <?php $applicant = \App\Applicant::find($id); ?>
      <div class="shape shape-style-1 bg-gradient-gray">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-12">
            <div class="card bg-secondary shadow border-0">
              <div class="card-header bg-white">
                <div class="text-muted">
                  <h3 class="heading">Edit Applicant<h3>
                </div>
              </div>
              <div class="card-body py-lg-5">
                <form role="form" action="/applicants/<?php echo $applicant->id; ?>" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  {{method_field('PUT')}}
                  <input value="{{$id}}" name="careerId" hidden>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Full Name</label>
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                          </div>
                          <input class="form-control" placeholder="Full Name" type="text" name="name" value="{{$applicant->name}}" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Email</label>
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                          </div>
                          <input class="form-control" placeholder="Email" type="email" name="email" value="{{$applicant->email}}" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Contact</label>
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-mobile-alt"></i></span>
                          </div>
                          <input class="form-control" placeholder="Contact" type="number" name="contactNo" value="{{$applicant->contactNO}}" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Status</label>
                        <div class="input-group input-group-alternative mb-3">
                          <select required name="status" class="form-control form-control-alternative">
                            @if($applicant->status=='Applied')
                            <option value="Applied" selected>Applied</option>
                            <option value="Contacted">Contacted</option>
                            <option value="Interviewed">Interviewed</option>
                            <option value="Hired">Hired</option>
                            <option value="Rejected">Rejected</option>
                            @elseif($applicant->status=='Contacted')
                            <option value="Applied">Applied</option>
                            <option value="Contacted" selected>Contacted</option>
                            <option value="Interviewed">Interviewed</option>
                            <option value="Hired">Hired</option>
                            <option value="Rejected">Rejected</option>
                            @elseif($applicant->status=='Interviewed')
                            <option value="Applied">Applied</option>
                            <option value="Contacted">Contacted</option>
                            <option value="Interviewed" selected>Interviewed</option>
                            <option value="Hired">Hired</option>
                            <option value="Rejected">Rejected</option>
                            @elseif($applicant->status=='Hired')
                            <option value="Applied">Applied</option>
                            <option value="Contacted">Contacted</option>
                            <option value="Interviewed">Interviewed</option>
                            <option value="Hired" selected>Hired</option>
                            <option value="Rejected">Rejected</option>
                            @elseif($applicant->status=='Rejected')
                            <option value="Applied">Applied</option>
                            <option value="Contacted">Contacted</option>
                            <option value="Interviewed">Interviewed</option>
                            <option value="Hired">Hired</option>
                            <option value="Rejected" selected>Rejected</option>
                            @endif
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>LinkedIn Profile</label>
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fab fa-linkedin"></i></span>
                          </div>
                          <input class="form-control" type="url" placeholder="LinkedIn Profile" name="linkedIn" value="{{$applicant->linkedInProfile}}">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Address</label>
                        <div class="input-group input-group-alternative mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                          </div>
                          <input class="form-control" placeholder="Address" type="text" name="address" value="{{$applicant->address}}" required>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Cover Letter</label>
                        <?php
                        $coverLetterUrl = "/applicant/coverLetter/"."/".$applicant->coverLetter;
                        $coverLetter = Storage::get($coverLetterUrl);
                        ?>

                        <textarea name="coverLetter" id="editor">{!!html_entity_decode($coverLetter)!!}</textarea>
                        <script>
                          ClassicEditor
                              .create( document.querySelector( '#editor' ) )
                              .catch( error => {
                                  console.error( error );
                              } );
                        </script>
                      </div>
                    </div>
                  </div>
                  <div class="text-right">
                    <input type="submit" class="btn btn-default mt-4" value="Apply">
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection
