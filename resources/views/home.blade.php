@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
  </div>
  <div class="col-md-12">
    <div class="card bg-secondary shadow">
      <div class="card-header bg-white border-0">
        <div class="row align-items-center">
          <div class="col-8">
            <h3 class="mb-0">About</h3>
          </div>

        </div>
      </div>
      <div class="card-body">
        <form>
          <div class="pl-lg-4">
            <div class="row">
              
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="form-control-label" for="input-email">Image/Videos</label>
                    <br>
                  <form action="/action_page.php">
                     <input type="file" name="myFile">
                  </form>
                  </div>
              </div>
            </div>
          </div>
          <hr class="my-4" />
          <!-- Save Button -->
            <div class="col-12 text-right">
            <a href="#!" class="btn btn-primary">Save</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
