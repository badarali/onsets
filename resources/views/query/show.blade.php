@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col-md-12">
          <div class="card">

            <div class="card-body">


              <h2 class="card-title">Query
                <div style="display:inline; float: right">
                  <form action="/queries/<?php echo $query->id;?>" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{method_field('PUT')}}
                  <select required name="status" class="form-control form-control-alternative">
                    @if($query->status == 'Open')
                    <option value="Open" selected>Open</option>
                    <option value="Close" >Close</option>
                    <option value="Processing" >Processing</option>
                    <option value="Decline" >Decline</option>

                    @elseif($query->status == 'Close')
                    <option value="Open" >Open</option>
                    <option value="Close" selected>Close</option>
                    <option value="Processing" >Processing</option>
                    <option value="Decline" >Decline</option>

                    @elseif($query->status == 'Processing')
                    <option value="Open" >Open</option>
                    <option value="Close" >Close</option>
                    <option value="Processing" selected>Processing</option>
                    <option value="Decline" >Decline</option>

                    @elseif($query->status == 'Decline')
                    <option value="Open" >Open</option>
                    <option value="Close" >Close</option>
                    <option value="Processing" >Processing</option>
                    <option value="Decline" selected >Decline</option>
                    @endif
                  </select>

                </div>
              </h2>
              <div class="row">
                <div class="col-md-3">
                  <p class="card-text"><b>Subject:</b></p>
                  <p class="card-text"><b>Email:</b></p>
                  <p class="card-text"><b>Status:</b></p>
                  <p class="card-text"><b>Query Date:</b></p>
                  <p class="card-text"><b>Query:</b></p>

                </div>
                <div class="col-md-9">
                  <p class="card-text">{{$query->subject}}</p>
                  <p class="card-text">{{$query->email}}</p>
                  <p class="card-text">{{$query->status}}</p>
                  <p class="card-text">{{$query->created_at}}</p>
                  <p class="card-text">
                  <?php
                  $queryUrl = "/query/"."/".$query->queryPath;
                  $userQuery = Storage::get($queryUrl);
                  ?>
                  {!!html_entity_decode($userQuery)!!}
                  </p>
                    <div style="display:inline; float: right">
                      <input class="btn btn-default" type="submit" value="Update Status"></input>
                    </div>
                  </form>

                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
