@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row">
              <div class="col-md-4">
              <h3 class="mb-0">Query  </h3>
            </div>
              <div class="col-md-8 text-right">
                <a href="{{ URL::to('/differnt','open') }}"  class="badge badge-success">Open</a>
                <a href="{{ URL::to('/differnt','close') }}" class="badge badge-warning">Close</a>
                <a href="{{ URL::to('/differnt','processing') }}" class="badge badge-info">Processing</a>
                <a href="{{ URL::to('/differnt','decline') }}" class="badge badge-danger">Decline</a>

  </div>
</div>
            </div>
            <?php

            ?>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Subect</th>
                    <th scope="col">Email</th>
                    <th scope="col">Status</th>
                    <th scope="col">Control Section</th>
                  </tr>
                </thead>
                <tbody>



                  @foreach($query as $info)
                  <tr>
                    <td>{{$info->id}}</td>
                    <th scope="row">
                      <div class="media align-items-center">

                        <div class="media-body">
                          <span class="mb-0 text-sm">{{$info->subject}}</span>
                        </div>
                      </div>
                    </th>
                    <th scope="row">
                      <div class="media align-items-center">

                        <div class="media-body">
                          <span class="mb-0 text-sm">{{$info->email}}</span>
                        </div>
                      </div>
                    </th>

                    <td>
                      <span class="badge badge-dot mr-4">
                        @if($info->status == 'Open')
                        <i class="bg-primary"></i> Open
                        @elseif($info->status == 'Close')
                        <i class="bg-success"></i> Close
                        @elseif($info->status == 'Processing')
                        <i class="bg-default"></i> Processing
                        @elseif($info->status == 'Decline')
                        <i class="bg-danger"></i> Decline
                        @endif
                      </span>
                    </td>
                    <td>
                        <a type="link" class="btn btn-success btn-sm" href="/queries/<?php echo $info->id; ?>">View</a>

                    </td>
                  </tr>


                  @endforeach

                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'<?php echo $color; ?>'
@endsection
@endif
