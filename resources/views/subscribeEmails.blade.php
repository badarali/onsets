@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">

              <h3 class="mb-0">Subcribe Email</h3>

            </div>

            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Email</th>
                    <th scope="col">Subcribe Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Control Section</th>
                  </tr>
                </thead>
                <tbody>



                  @foreach($data as $info)
                  <tr>
                    <td>{{$info->id}}</td>
                    <th scope="row">
                      <div class="media align-items-center">

                        <div class="media-body">
                          <span class="mb-0 text-sm">{{$info->email}}</span>
                        </div>
                      </div>
                    </th>
                    <td>{{$info->created_at}}</td>
                    <td>
                      <span class="badge badge-dot mr-4">
                        @if($info->status == 'Deactive')
                        <i class="bg-warning"></i> Deactive
                        @elseif($info->status == 'Active')
                        <i class="bg-success"></i> Active
                        @endif
                      </span>
                    </td>
                    <td>
                      <form action="/subcribeEmails/<?php echo $info->id;?>" method="post" >
                        {{ csrf_field() }}
                        {{method_field('PUT')}}
                        <input class="btn btn-default btn-sm" type="submit" value="Change Status"></input>
                      </form>
                    </td>
                  </tr>


                  @endforeach

                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'<?php echo $color; ?>'
@endsection
@endif
