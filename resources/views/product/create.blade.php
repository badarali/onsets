@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Product</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form action="/products" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div>
                  <div class="row">
                      <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Title</label>
                        <input required type="text" id="input-postal-code" name="title" class="form-control form-control-alternative" placeholder="Title">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Link</label>
                        <input required name="url" type="url" id="input-postal-code" class="form-control form-control-alternative" placeholder="Link">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Status</label>
                        <select required name="status" class="form-control form-control-alternative">
                          <option value="Active">Active</option>
                          <option value="Deactive">Deactive</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Description</label>
                        <textarea name="editor1" class="form-control" required placeholder="Description" rows="5"></textarea>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Primary Image <small>(Only PNG, JPG, JPEG formats allowed)</small></label>
                        <label class="btn btn-success" for="image"><i class="fas fa-upload"></i> Upload Image</label><small id="imageLabel"></small>
                        <input id="image" style="position:absolute; opacity:0; z-index:-1" type="file" name="image" required class="form-control form-control-alternative" accept=".jpg,.jpeg,.png">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Images <small>(Only PNG, JPG, JPEG formats allowed)</small></label><br>
                        <label class="btn btn-success" for="images"><i class="fas fa-upload"></i> Upload Images</label><small id="imagesLabel"></small>
                        <input type="file" id="images" style="position:absolute; opacity:0; z-index:-1" name="images[]" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png" multiple>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Videos <small>(Only FLV, MP4, M4P, MPEG, MOV formats allowed)</small></label>
                        <label class="btn btn-success" for="videos"><i class="fas fa-upload"></i> Upload Videos</label><small id="videosLabel"></small>
                        <input type="file" id="videos" style="position:absolute; opacity:0; z-index:-1" name="videos[]" class="form-control form-control-alternative" accept=".mp4,.m4p,.mpeg,.webm,.mov,.mpg,.flv" multiple>
                      </div>
                    </div>
                    <div class="col-md-12 text-right">
                      <input class="btn btn-default" type="submit" value="Save"></input>
                    </div>
                  </div>
                </div>
                <!-- Save Button -->
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
