@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <?php
            $product = \App\Product::find($id);
            ?>
            <img class="card-img-top" src="{{asset('product/images/').'/'.$product->productImage}}" height="500em" alt="Card image cap">
            <?php
            $media = \App\Media::where('for', 'product')->where('forId', $product->id)->get();
            ?>
            @if(!$media->isEmpty())
            <div class="row text-center mt-4">
              @foreach($media as $value)
              @if($value->type == 'image')
              <div class="col-md-3">
                <img class="img img-responsive col-md-12" src="{{asset('product/images/').'/'.$value->name}}">
                <small class="col-md-12 text-center">{{$value->name}}</small>
                <form action="{{ route('media.destroy', $value->id) }}" method="post" style="display:inline">
                          {{ method_field('DELETE') }}
                          {{ csrf_field() }}
                          <input hidden value="{{$product->id}}" name="forId">
                          <input hidden value="product" name="type">
                  <input type="submit" class="btn btn-danger btn-sm mt-2 mb-2" placeholder="Delete" value="Delete"/>
                </form>
              </div>
              @elseif($value->type == 'video')
              <div class="col-md-3">
                <video class="video col-md-12 mb-4" controls>
                  <source src="{{asset('product/videos/').'/'.$value->name}}">
                </video>
                <small class="col-md-12 text-center">{{$value->name}}</small>
                <form action="{{ route('media.destroy', $value->id, $product->id, 'product') }}" method="post" style="display:inline">
                  <input hidden value="{{$product->id}}" name="forId">
                  <input hidden value="product" name="type">
                          {{ method_field('DELETE') }}
                          {{ csrf_field() }}
                  <input type="submit" class="btn btn-danger btn-sm mt-2 mb-2" placeholder="Delete" value="Delete"/>
                </form>
              </div>
              @endif
              @endforeach
            </div>
            @endif
            <div class="card-body">
              <form action="/products/<?php echo $product->id;?>" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div>
                  <div class="row">
                      <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Title</label>
                        <input required type="text" id="input-postal-code" name="title" value="{{$product->productName}}" class="form-control form-control-alternative" placeholder="Title">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Link</label>
                        <input value="{{$product->productURL}}" required name="url" type="text" id="input-postal-code" class="form-control form-control-alternative" placeholder="Link">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Status</label>
                        <select required name="status" class="form-control form-control-alternative">
                          @if($product->status=='Active')
                          <option value="Active" selected>Active</option>
                          <option value="Deactive">Deactive</option>
                          @elseif($product->status=='Deactive')
                          <option value="Active">Active</option>
                          <option value="Deactive" selected>Deactive</option>
                          @endif
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Description</label>
                        <?php
                        $descriptionUrl = "/product/"."/".$product->description;
                        $description = Storage::get($descriptionUrl);
                        ?>
                        <textarea class="form-control" rows="5" name="editor1" value="{{$product->description}}" required placeholder="Description">{!!html_entity_decode($description)!!}</textarea>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Primary Image <small>(Only PNG, JPG, JPEG formats allowed)</small></label>
                        <label class="btn btn-success" for="image"><i class="fas fa-upload"></i> Upload Image</label><small id="imageLabel"></small>
                        <input type="file" id="image" style="position:absolute; opacity:0; z-index:-1" name="image" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Images <small>(Only PNG, JPG, JPEG formats allowed)</small></label><br>
                        <label class="btn btn-success" for="images"><i class="fas fa-upload"></i> Upload Images</label><small id="imagesLabel"></small>
                        <input type="file" id="images" style="position:absolute; opacity:0; z-index:-1" name="images[]" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png" multiple>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Videos <small>(Only FLV, MP4, M4P, MPEG, MOV formats allowed)</small></label>
                        <label class="btn btn-success" for="videos"><i class="fas fa-upload"></i> Upload Videos</label><small id="videosLabel"></small>
                        <input type="file" id="videos" style="position:absolute; opacity:0; z-index:-1" name="videos[]" class="form-control form-control-alternative" accept=".mp4,.m4p,.mpeg,.webm,.mov,.mpg,.flv" multiple>
                      </div>
                    </div>
                    <div class="col-md-12 text-right">
                      <input class="btn btn-default" type="submit" value="Update"></input>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
