@extends('layouts.app')

@section('content')
<div class="card" class="col-md-12">
  <img class="card-img-top" src="{{asset('product/images/').'/'.$product->productImage}}" height="500em" alt="Card image cap">
  <div class="card-body">
    <h2 class="card-title">{{$product->productName}} <small>({{$product->status}})</small>
      <?php
      $uploader = \App\User::find($product->uploaderID);
      ?>
      <div style="display:inline; float: right">
      <small><b>Uploaded By:</b> {{$uploader->name}}</small></div>
    </h2>
    <p><a href="{{$product->productURL}}" target="_new">View {{$product->productName}}</a></p>
    <p class="card-text" id="desc">
    <?php
    $descriptionUrl = "/product/"."/".$product->description;
    $description = Storage::get($descriptionUrl);
    ?>
    {!!html_entity_decode($description)!!}
    </p>
  </div>
</div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
