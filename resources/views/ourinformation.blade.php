@extends('layouts.app')

@section('content')
<?php
$id='';
$email='';
$phoneNO='';
$address='';
$facebook='';
$twitter='';
$linkedIn='';
$gitHub='';
$googlePlus='';
$location='';
$buttonValue='Save';
$aboutus = "";
$formaction='/ourinfoAdd';
?>
<div class="row">
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Our Infomation</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <?php
              if(!empty($info)){
                $media = \App\Media::where('for', 'aboutus')->where('forId', $info->id)->get();
              }
              ?>
              @if(!empty($info) && !$media->isEmpty())
              <div class="row text-center mt-4">
                @foreach($media as $value)
                @if($value->type == 'image')
                <div class="col-md-3">
                  <img class="img img-responsive col-md-12" src="{{asset('aboutus/images/').'/'.$value->name}}">
                  <small class="col-md-12 text-center">{{$value->name}}</small>
                  <form action="{{ route('media.destroy', $value->id) }}" method="post" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <input hidden value="{{$info->id}}" name="forId">
                            <input hidden value="aboutus" name="type">
                    <input type="submit" class="btn btn-danger btn-sm mt-2 mb-2" placeholder="Delete" value="Delete"/>
                  </form>
                </div>
                @elseif($value->type == 'video')
                <div class="col-md-3">
                  <video class="video col-md-12 mb-4" controls>
                    <source src="{{asset('aboutus/videos/').'/'.$value->name}}">
                  </video>
                  <small class="col-md-12 text-center">{{$value->name}}</small>
                  <form action="{{ route('media.destroy', $value->id, $info->id, 'aboutus') }}" method="post" style="display:inline">
                    <input hidden value="{{$info->id}}" name="forId">
                    <input hidden value="aboutus" name="type">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger btn-sm mt-2 mb-2" placeholder="Delete" value="Delete"/>
                  </form>
                </div>
                @endif
                @endforeach
              </div>
              @endif
              @if(!empty($info))
              <?php
              $id=$info->id;
              $email=$info->email;
              $phoneNO=$info->phoneNO;
              $address=$info->address;
              $facebook=$info->facebook;
              $twitter=$info->twitter;
              $linkedIn=$info->linkedIn;
              $gitHub=$info->gitHub;
              $googlePlus=$info->googlePlus;
              $location=$info->location;
              $aboutusUrl = "/aboutus/"."/".$info->aboutus;
              $aboutus = Storage::get($aboutusUrl);
              $buttonValue='Update';
              $formaction='/ourinfoUpdate/'.$id;
              ?>
              @endif

              <form action="{{ $formaction }}" method="post" enctype="multipart/form-data">

                {{ csrf_field() }}
                <input type="hidden" value=" {{ $id }}" name="id">
                <div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Phone</label>
                        <input type="tel" id="input-username" name="phone" class="form-control form-control-alternative" placeholder="0300:0000000"  value="{{ $phoneNO }}" required>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Email address</label>
                        <input type="email" id="input-email" name="email" class="form-control form-control-alternative" placeholder="jesse@example.com" value="{{$email }}" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Address</label>
                        <input type="text" id="input-first-name" name="address" class="form-control form-control-alternative" placeholder="Address" value="{{ $address }}" required>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Social Media Links -->
                <h6 class="heading-small text-muted mb-4">Social Media Link</h6>
                <div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Facebook</label>
                        <input id="input-address" name="facebook" class="form-control form-control-alternative" placeholder="facebook.com"  type="url" value="{{ $facebook }}" >
                      </div>
                    </div>
                       <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Twitter</label>
                        <input id="input-address" name="twitter" class="form-control form-control-alternative" placeholder="Twitter.com"  type="url" value="{{ $twitter }}" >
                      </div>
                    </div>
                       <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">LinkedIn</label>
                        <input id="input-address" name="linkenIn" class="form-control form-control-alternative" placeholder="LinkedIn.com"  type="url" value="{{ $linkedIn }}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-city">GitHub</label>
                        <input type="url" id="input-city"name="gitHub" class="form-control form-control-alternative" placeholder="GitHub.com" value="{{ $gitHub }}">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">GooglePlus</label>
                        <input type="url" id="input-country" name="goolePlus" class="form-control form-control-alternative" placeholder="GooglePlus.com" value="{{ $googlePlus }}">
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-username">About Us</label>
                    <textarea rows="8" class="form-control" name="editor1" placeholder="About Us">{!!html_entity_decode($aboutus)!!}</textarea>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-email">About Us Media <small>(Only PNG, JPG, JPEG formats allowed)</small></label>
                       <input type="file" name="image" class="form-control form-control-alternative">
                    </div>
                </div>
                <!-- Our Location -->
                <div class="col-md-12">
                  <div class="form-group">
                   <label class="form-control-label" for="input-country">Loction</label>
                        <input type="text" name="location" id="input-postal-code" class="form-control form-control-alternative" placeholder="Our Location Link" value="{{ $location }}">
                  </div>
                </div>
                   <div class="col-md-12 text-right">
                    <input class="btn btn-default" type="submit" value="{{ $buttonValue }}"></input>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
