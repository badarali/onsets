@extends('layouts.app')

@section('content')
<div class="card" class="col-md-12">
  <img class="card-img-top" src="{{asset('story/images/').'/'.$story->publishImage}}" height="500em" alt="story Image">
  <div class="card-body">
    <?php
    if ($story->visibleFront =='1') {  $visbleStatus="Visible";}
    else{ $visbleStatus  ="Invisible"; }
     ?>

    <h2 class="card-title">{{$story->publishTitle}} <small>({{$story->status }}/{{ $visbleStatus }})</small>
      <?php
      $uploader = \App\User::find($story->uploaderID);
      ?>
      <div style="display:inline; float: right">
      <small><b>Uploaded Info:</b> {{ $uploader->name }} ({{ $story->publishDate}})</small></div>
    </h2>
    <p class="card-text" id="desc">
    <?php
    $descriptionUrl = "/story/"."/".$story->descriptionPath;
    $description = Storage::get($descriptionUrl);
    ?>
    {!!html_entity_decode($description)!!}
    </p>

  </div>
</div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
