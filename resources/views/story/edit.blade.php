@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <?php
            $story = \App\Story::find($id);
            ?>
            <img class="card-img-top" src="{{asset('story/images/').'/'.$story->publishImage}}" height="500em" alt="Card image cap">
            <?php
            $media = \App\Media::where('for', 'story')->where('forId', $story->id)->get();
            ?>
            <div class="card-body">
              @if(!$media->isEmpty())
              <div class="row text-center mt-4">
                @foreach($media as $value)
                @if($value->type == 'image')
                <div class="col-md-3">
                  <img class="img img-responsive col-md-12" src="{{asset('story/images/').'/'.$value->name}}">
                  <form action="{{ route('media.destroy', $value->id) }}" method="post" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <input hidden value="{{$story->id}}" name="forId">
                            <input hidden value="story" name="type">
                    <input type="submit" class="btn btn-danger btn-sm mt-2 mb-2" placeholder="Delete" value="Delete"/>
                  </form>
                </div>
                @elseif($value->type == 'video')
                <div class="col-md-3">
                  <video class="video col-md-12 mb-4" controls>
                    <source src="{{asset('story/videos/').'/'.$value->name}}">
                  </video>
                  <form action="{{ route('media.destroy', $value->id, $story->id, 'story') }}" method="post" style="display:inline">
                    <input hidden value="{{$story->id}}" name="forId">
                    <input hidden value="story" name="type">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger btn-sm mt-2 mb-2" placeholder="Delete" value="Delete"/>
                  </form>
                </div>
                @endif
                @endforeach
              </div>
              @endif
              <form action="/stories/<?php echo $story->id;?>" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <input type="hidden" value="{{ $story->id}}" name="id">
                <div>
                  <div class="row">
                      <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Publish Title</label>
                        <input type="text" required id="input-postal-code" name="name" class="form-control form-control-alternative"  value="{{ $story->publishTitle }}">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Publish Date</label>
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                            </div>
                            <input class="form-control datepicker" name="publishDate" placeholder="Select date" type="text" value="{{$story->publishDate}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Status</label>
                        <select required name="status" class="form-control form-control-alternative">
                          @if($story->status=='Active')
                          <option value="Active" selected>Active</option>
                          <option value="Deactive">Deactive</option>
                          @elseif($story->status=='Deactive')
                          <option value="Active">Active</option>
                          <option value="Deactive" selected>Deactive</option>
                          @endif
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Visible Front</label>
                        <select required name="frontVisible" class="form-control form-control-alternative">
                          @if($story->visibleFront=='1')
                          <option value="1" selected>Visble</option>
                          <option value="0">Invisble</option>
                          @elseif($story->visibleFront=='0')
                          <option value="1">Visble</option>
                          <option value="0" selected>Invisble</option>
                          @endif
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Primary Image <small>(Only PNG, JPG, JPEG formats allowed)</small></label>
                        <label class="btn btn-success" for="image"><i class="fas fa-upload"></i> Upload Image</label><small id="imageLabel"></small>
                        <input type="file" id="image" style="position:absolute; opacity:0; z-index:-1" name="image" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Images <small>(Only PNG, JPG, JPEG formats allowed)</small></label><br>
                        <label class="btn btn-success" for="images"><i class="fas fa-upload"></i> Upload Images</label><small id="imagesLabel"></small>
                        <input type="file" id="images" style="position:absolute; opacity:0; z-index:-1" name="images[]" class="form-control form-control-alternative" accept=".jpg,.jpeg,.png" multiple>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Videos <small>(Only FLV, MP4, M4P, MPEG, MOV formats allowed)</small></label>
                        <label class="btn btn-success" for="videos"><i class="fas fa-upload"></i> Upload Videos</label><small id="videosLabel"></small>
                        <input type="file" id="videos" style="position:absolute; opacity:0; z-index:-1" name="videos[]" class="form-control form-control-alternative" accept=".mp4,.m4p,.mpeg,.webm,.mov,.mpg,.flv" multiple>
                      </div>
                    </div>
                       <div class="col-md-12">
                           <div class="form-group">
                             <?php
                             $descriptionUrl = "/story/"."/".$story->descriptionPath;
                             $description = Storage::get($descriptionUrl);
                             ?>

                             <label class="form-control-label" for="input-username" >Story Description</label>
                             <textarea class="form-control" rows="5" name="editor1" value="{{$story->descriptionPath}}" required>{!!html_entity_decode($description)!!}</textarea>
                           </div>
                         </div>


                    </div>
                </div>
                <!-- Save Button -->
                <div class="row">
                  <div class="col-md-12 text-right">
                    <input class="btn btn-default" type="submit" value="Update"></input>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
