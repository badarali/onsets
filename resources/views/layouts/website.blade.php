<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Start your development with a Design System for Bootstrap 4.">
        <meta name="author" content="Creative Tim">
        <title>OnSets - A Software Company</title>
        <!-- Favicon -->
        <link href="{{ asset('./assets/img/brand/blue.png') }}" rel="icon" type="image/png">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <!-- Icons -->
        <link href="{{ asset('./assets/vendorAlpha/nucleo/css/nucleo.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link href="{{ asset('./assets/vendorAlpha/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('./assets/css/argon.css?v=1.0.1') }}" rel="stylesheet">
        <!-- Docs CSS -->
      
        <link rel="stylesheet" href="{{ asset('./assets/css/style.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.min.css" type="text/css">
        <script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script>
    </head>
    <body>
        <header class="header-global" id="home">
            <nav id="navbar-main" class="navbar navbar-main navbar-transparent navbar-expand-lg navbar-dark bg-default headroom">
                <div class="container">
                    <a class="navbar-brand mr-lg-5" href="/">
                        <img src="{{ asset('./assets/img/brand/white.png') }}">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-success" aria-controls="navbar-success" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbar-success">
                        <div class="navbar-collapse-header">
                            <div class="row">
                                <div class="col-6 collapse-brand">
                                  <a href="#">
                                    <img src="{{ asset('./assets/img/brand/blue.png') }}">
                                  </a>
                                </div>
                                <div class="col-6 collapse-close">
                                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-success" aria-controls="navbar-success" aria-expanded="false" aria-label="Toggle navigation">
                                        <span></span>
                                        <span></span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        @if (!empty($message))
                          <ul class="navbar-nav ml-lg-auto">
                              <li class="nav-item">
                                  <a class="nav-link" href="#home" title="Home">Home
                                      <span class="sr-only">(current)</span>
                                  </a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="#about" title="About">About</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="#custom" title="Custom-Line">Custom-Line</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="#product" title="Product-Line">Product-Line</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="#story" title="Story">Story</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="#career" title="Career">Career</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" href="#contact" title="Contact">Contact</a>
                              </li>
                          </ul>
                          @else
                            <ul class="navbar-nav ml-lg-auto">
                              <li class="nav-item">
                                <a class="btn btn-neutral" href="{{ url('/') }}" title="Home"><i class="fas fa-chevron-left"></i> Back To Home</a>
                              </li>
                            </ul>
                          @endif
                    </div>
                </div>
            </nav>
        </header>
        @yield('content')

        <footer class="footer bg-default text-center text-white pb-0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-sm-12 text-center animLeft">
                        <h3 class="heading text-white revealer para"><b>Our</b> Information</h3>
                        <br>
                        <ul class="list-unstyled">

                          <?php
                          $ourInfo= \App\Ourinformation::first();
                          ?>
                          @if(!empty($ourInfo->phoneNO))
                            <li style="padding: 4%; margin-bottom: 3%; background-color: #29426d; border-radius: .25rem;"><i class="fas fa-phone"></i>&nbsp;&nbsp;&nbsp;{{ $ourInfo->phoneNO }} </li>
                          @endif
                          @if(!empty($ourInfo->email))
                            <li style="padding: 4%; margin-bottom: 3%; background-color: #29426d; border-radius: .25rem;"><i class="fas fa-envelope"></i>&nbsp;&nbsp;&nbsp;{{ $ourInfo->email}}</li>
                          @endif
                          @if(!empty($ourInfo->address))
                            <li style="padding: 4%; margin-bottom: 3%; background-color: #29426d; border-radius: .25rem;"><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp;&nbsp;{{ $ourInfo->address}} </li>
                            <!-- <li style="padding: 4%; margin-bottom: 3%; background-color: #29426d; border-radius: .25rem;"><i class="fas fa-envelope"></i>&nbsp;&nbsp;&nbsp;info@onsets.co </li> -->
                          @endif
                        </ul>
                    </div>
                    <div class="col-lg-4 text-center revealer para">
                        <h3 class="heading text-white revealer para"><b>Site</b>map</h3>
                        <img src="{{ asset('./assets/img/brand/white.png') }}"  class="img img-responsive col-md-12"/>
                        <ul class="list-unstyled" style="margin-top: 7%;">
                            <li class="list-inline-item"><a href="#home" class="text-white">Home</a></li>
                            <li class="list-inline-item"><a href="#about" class="text-white">About</a></li>
                            <li class="list-inline-item"><a href="#product" class="text-white">Prouct-Line</a></li>
                            <li class="list-inline-item"><a href="#custom" class="text-white">Custom-Line</a></li>
                            <li class="list-inline-item"><a href="#story" class="text-white">Stories</a></li>
                            <li class="list-inline-item"><a href="#career" class="text-white">Career</a></li>
                            <li class="list-inline-item"><a href="#contact" class="text-white">Contact</a></li>
                            @if (Route::has('login'))
                              @auth
                              <li class="list-inline-item"><a class="text-white" href="{{ url('/home') }}" title="Dashboard">Dashboard</a>
                              @else
                              <li class="list-inline-item" hidden><a href="{{ route('login') }}" class="text-white">Login</a></li>
                              <!-- <li class="list-inline-item" hidden><a href="{{ route('register') }}" class="text-white">Register</a></li> -->
                              @endauth
                            @endauth
                        </ul>
                    </div>
                    <div class="col-lg-4 text-center anim">
                        <h3 class="heading text-white"><b>Socialize</b> With Us</h3>
                        <br>
                          @if(!empty($ourInfo->twitter))
                        <a target="_blank" href="{{ $ourInfo->twitter}}" class="btn btn-neutral btn-icon-only btn-twitter btn-round btn-lg" data-toggle="tooltip" data-original-title="Follow us">
                            <i class="fa fa-twitter"></i>
                        </a>
                        @endif
                          @if(!empty($ourInfo->linkedIn))
                        <a target="_blank" href="{{ $ourInfo->linkedIn}}" class="btn btn-neutral btn-icon-only btn-linkedin btn-round btn-lg" data-toggle="tooltip" data-original-title="Follow us">
                            <i class="fab fa-linkedin"></i>
                        </a>
                        @endif
                          @if(!empty($ourInfo->facebook))
                        <a target="_blank" href="{{ 	$ourInfo->facebook}}" class="btn btn-neutral btn-icon-only btn-facebook btn-round btn-lg" data-toggle="tooltip" data-original-title="Like us">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        @endif
                          @if(!empty($ourInfo->googlePlus))
                        <a target="_blank" href="{{ 	$ourInfo->googlePlus}}" class="btn btn-neutral btn-icon-only btn-dribbble btn-lg btn-round" data-toggle="tooltip" data-original-title="Follow us">
                            <i class="fab fa-google-plus-square"></i>
                        </a>
                        @endif
                          @if(!empty($ourInfo->gitHub))
                        <a target="_blank" href="{{ $ourInfo->gitHub}}" class="btn btn-neutral btn-icon-only btn-github btn-round btn-lg" data-toggle="tooltip" data-original-title="Star on Github">
                            <i class="fa fa-github"></i>
                        </a>
                        @endif
                        <hr style="border-top: .0625rem solid rgb(255, 255, 255);">
                        <div class="col-md-12">
                            <h3 class="heading text-white"><b>Subscribe</b> NewsLetter</h3>
                            <br>
                            <form role="form" method="post" action="\subcribeEmails">
                                {{ csrf_field() }}
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Email" type="email" name="email">
                                    <button type="submit" class="btn btn-inline btn-success">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3 class="description text-white text-center">&copy;
                            <script type="text/javascript">
                                var dt = new Date();
                                document.write(dt.getFullYear() );
                            </script>
                            by <a href="http://www.onsets.co" class="text-success">ONSETS</a>. All rights reserved | Design by
                            <a href="https://www.creative-tim.com/" class="text-success">creative-tim</a>
                        </h3>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Core -->
        <a class="btn btn-success" href="#home" style="position: fixed; bottom: 2%; right: 2%;" title="Go To Top"><i style="font-size: 25px" class="fas fa-long-arrow-alt-up"></i></a>
        <script src="{{ asset('./assets/vendorAlpha/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('./assets/vendorAlpha/popper/popper.min.js') }}"></script>
        <script src="{{ asset('./assets/vendorAlpha/bootstrap/bootstrap.min.js') }}"></script>
        <script src="{{ asset('./assets/vendorAlpha/headroom/headroom.min.js') }}"></script>
        <script src="{{ asset('./assets/js/demo.js') }}" type="text/javascript"></script>
        <!-- Optional JS -->
        <script src="{{ asset('./assets/vendorAlpha/onscreen/onscreen.min.js') }}"></script>
        <script src="{{ asset('./assets/vendorAlpha/nouislider/js/nouislider.min.js') }}"></script>
        <script src="{{ asset('./assets/vendorAlpha/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <!-- Argon JS -->
        <script src="{{ asset('./assets/js/argon.js?v=1.0.1') }}"></script>
        <script>
            $(document).ready(function(){
              // Add smooth scrolling to all links
              $("a").on('click', function(event) {

                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                  // Prevent default anchor click behavior
                  event.preventDefault();

                  // Store hash
                  var hash = this.hash;

                  // Using jQuery's animate() method to add smooth page scroll
                  // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                  $('html, body').animate({
                    scrollTop: $(hash).offset().top
                  }, 1300, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                  });
                } // End if
              });
            });
        </script>


        <script src="{{ asset('./js/bootstrap-notify.min.js') }}"></script>

        <script type="text/javascript">
                $(document).ready(function(){
                    $.notify({
                        icon: @yield('icon'),
                        message: @yield('message')
                      },{
                          type: @yield('barcolor'),
                          timer: 4000
                      });
                });
            </script>
            <script type="text/javascript" src="{{ asset('./js/chooseFile.js') }}"></script>
    </body>
</html>
