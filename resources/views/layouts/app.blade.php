<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>OnSets - A Software Company</title>
  <!-- Favicon -->
  <link href="{{ asset('./img/brand/blue.png') }}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('./vendorAlpha/nucleo/css/nucleo.css') }}" rel="stylesheet">
  <link href="{{ asset('./vendorAlpha/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('./css/argon.css?v=1.0.0') }}" rel="stylesheet">
  <script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script>
</head>

<body>
  <!-- Sidenav -->
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-default" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="/home" style="border-bottom: 1px solid white">
        <img src="{{ asset('./img/brand/white.png') }}" class="navbar-brand-img" alt="...">
      </a>
      <!-- User -->
      <ul class="nav align-items-center d-md-none">
        <li class="nav-item dropdown">
          <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="ni ni-bell-55"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="{{ asset('./img/theme/team-1-800x800.jpg') }}">
              </span>
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <div class=" dropdown-header noti-title">
              <h6 class="text-overflow m-0">Welcome!</h6>
            </div>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-single-02"></i>
              <span>My profileytry</span>
            </a>

            <div class="dropdown-divider"></div>
            <a href="logout" class="dropdown-item">
              <i class="ni ni-user-run"></i>
              <span>Logout</span>
            </a>
          </div>
        </li>
      </ul>
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="/" style="border-bottom: 1px solid white">
                <img src="{{ asset('./img/brand/white.png') }}">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Form -->
        <form class="mt-4 mb-3 d-md-none">
          <div class="input-group input-group-rounded input-group-merge">
            <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
          </div>
        </form>
        <!-- Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link text-white" href="/sliders">
              <i class="fas fa-sliders-h"></i>Sliders
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/ourinfo">
              <i class="fas fa-info-circle"></i> Our Information
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/projects">
              <i class="fas fa-project-diagram"></i> Projects
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/products">
              <i class="fab fa-product-hunt"></i> Products
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/stories">
              <i class="fab fa-stripe-s"></i> Story
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/careers">
              <i class="fab fa-avianex"></i> Careers
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/queries">
              <i class="fas fa-question-circle"></i>
              Query
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/subcribeEmails">
              <i class="fas fa-envelope-square"></i>
              Subcribe Email
            </a>
          </li>
          <li class="nav-item">
              <a class="nav-link text-white" href="/roles">
                <i class="fab fa-r-project"></i>Role
              </a>
          </li>
          <li class="nav-item">
              <a class="nav-link text-white" href="/users">
                <i class="fas fa-users"></i>User
              </a>
          </li>




        </ul>
        <!-- Divider -->
        <!-- Heading -->

      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->


        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">

        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="{{ asset('./img/brand/favicon.png') }}">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold " style="color: black">@if(!empty(Auth::User()->id)) <?php echo Auth::User()->name ?> @endif</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              @if(!empty(Auth::User()->id))
              <a href="/users/<?php echo Auth::User()->id ?> " class="dropdown-item">
                @endif
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>

              <div class="dropdown-divider"></div>
              <a href="logout" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- Header -->
    <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 300px; background-image:url('{{ asset('./img/brand/blue.png') }}'); background-size: cover; background-position: center top;">

    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      @yield('content')
      <!-- Footer -->
      <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              &copy; 2018 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
            </div>
          </div>
          <div class="col-xl-6">
          
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('./vendorAlpha/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('./vendorAlpha/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('./js/bootstrap-notify.min.js') }}"></script>

  <script type="text/javascript">
          $(document).ready(function(){
              $.notify({
                  icon: @yield('icon'),
                  message: @yield('message')
                },{
                    type: @yield('barcolor'),
                    timer: 4000
                });
          });
      </script>
  <!-- Argon JS -->
  <script src="{{ asset('/vendorAlpha/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script>
    $('.datepicker').datepicker({
      format: 'yyyy-mm-dd',
    });
  </script>
  <script src="{{ asset('./js/argon.js?v=1.0.0') }}"></script>
  <script type="text/javascript" src="{{ asset('./js/chooseFile.js') }}"></script>
</body>
</html>
