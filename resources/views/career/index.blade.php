@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Careers
                <div style="display: inline; float: right" >
                  <a type="link" class="btn btn-sm btn-primary pull-right" href="{{URL::asset('careers/create')}}">Add Job</a>
                </div>
              </h3>
            </div>
            <?php
            $careers = \App\Career::all()->sortByDesc("id");
            ?>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Title</th>
                    <th scope="col">Area</th>
                    <th scope="col">Due Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Control Section</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($careers as $info)
                  <tr>
                    <td>{{$info->id}}</td>
                    <th scope="row">
                      <div class="media align-items-center">
                        <div class="media-body">
                          <span class="mb-0 text-sm">{{$info->title}}</span>
                        </div>
                      </div>
                    </th>
                    <td>{{$info->area}}</td>
                    <td>{{$info->dueDate}}</td>
                    <td>
                      <span class="badge badge-dot mr-4">
                        @if($info->status == 'Deactive')
                        <i class="bg-warning"></i> Deactive
                        @elseif($info->status == 'Active')
                        <i class="bg-success"></i> Active
                        @endif
                      </span>
                    </td>
                    <td>
                        <a type="link" class="btn btn-success btn-sm" href="/careers/<?php echo $info->id; ?>">View</a>
                        <a type="link" class="btn btn-default btn-sm" href="/careers/<?php echo $info->id;?>/edit">Edit</a>
                        <a type="link" class="btn btn-info btn-sm" href="/viewApplicants/<?php echo $info->id;?>">Applicants</a>
                        <form action="{{ route('careers.destroy', $info->id) }}" method="post" style="display:inline">
                                  {{ method_field('DELETE') }}
                                  {{ csrf_field() }}
                          <input type="submit" class="btn btn-danger btn-sm" placeholder="Delete" value="Delete"/>
                        </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
@if(!empty($color))
'<?php echo $color; ?>'
@endif
@endsection
@endif
