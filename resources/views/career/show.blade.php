@extends('layouts.app')
@section('content')
<div class="row">
  <?php
  $career = \App\Career::find($id);
  ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <h2 class="card-title">{{$career->title}} <small>({{$career->status}})</small></h2>
              <div class="row">
                <div class="col-md-6">
                  <p class="card-text"><b>Area:</b></p>
                  <p class="card-text"><b>Experience:</b></p>
                  <p class="card-text"><b>Number Of Positions:</b></p>
                  <p class="card-text"><b>Education:</b></p>
                  <p class="card-text"><b>Uploaded Date:</b></p>
                  <p class="card-text"><b>Uploaded By:</b></p>
                  <p class="card-text"><b>Due Date:</b></p>
                  <p class="card-text"><b>Description:</b></p>
                </div>
                <div class="col-md-6">
                  <p class="card-text">{{$career->area}}</p>
                  <p class="card-text">{{$career->experience}}</p>
                  <p class="card-text">{{$career->noOfPosition}}</p>
                  <p class="card-text">{{$career->education}}</p>
                  <p class="card-text">{{$career->created_at}}</p>
                  <?php
                  $uploader = \App\User::find($career->uploaderId);
                  ?>
                  <p class="card-text">{{$uploader->name}}</p>
                  <p class="card-text">{{$career->dueDate}}</p>
                </div>
                <div class="col-md-12">
                  <p class="card-text">
                  <?php
                  $descriptionUrl = "/career/"."/".$career->descriptionPath;
                  $description = Storage::get($descriptionUrl);
                  ?>
                  {!!html_entity_decode($description)!!}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
