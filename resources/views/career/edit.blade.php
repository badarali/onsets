@extends('layouts.app')
@section('content')
<div class="row">
  <?php
  $career = \App\Career::find($id);
  ?>
        <div class="col-md-12">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Career</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form action="/careers/<?php echo $career->id;?>" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div>
                  <div class="row">
                      <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Title</label>
                        <input value="{{$career->title}}" required type="text" id="input-postal-code" name="title" class="form-control form-control-alternative" placeholder="Title">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Area</label>
                        <input value="{{$career->area}}" required name="area" type="text" id="input-postal-code" class="form-control form-control-alternative" placeholder="Area">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Due Date</label>
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                            </div>
                            <input class="form-control datepicker" value="{{$career->dueDate}}" name="dueDate" placeholder="Select date" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Number Of Positions</label>
                        <input value="{{$career->noOfPosition}}" required name="noOfPositions" type="number" id="input-postal-code" class="form-control form-control-alternative" placeholder="Number Of Positions" min="1">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Experience</label>
                        <input value="{{$career->experience}}" required name="experience" type="text" id="input-postal-code" class="form-control form-control-alternative" placeholder="Experience">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Education</label>
                        <input value="{{$career->education}}" required name="education" type="text" id="input-postal-code" class="form-control form-control-alternative" placeholder="Education">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Status</label>
                        <select required name="status" class="form-control form-control-alternative">
                          @if($career->status=='Active')
                          <option value="Active" selected>Active</option>
                          <option value="Deactive">Deactive</option>
                          @elseif($career->status=='Deactive')
                          <option value="Active">Active</option>
                          <option value="Deactive" selected>Deactive</option>
                          @endif
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Visible</label>
                        <select required name="visible" class="form-control form-control-alternative">
                          <option value="1">Visible</option>
                          <option value="0">Invisible</option>
                        </select>
                      </div>
                    </div>
                    <?php
                    $descriptionUrl = "/career/"."/".$career->descriptionPath;
                    $description = Storage::get($descriptionUrl);
                    ?>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Description</label>
                        <textarea name="editor1" required class="form-control" rows="8" placeholder="Description">{!!html_entity_decode($description)!!}</textarea>
                      </div>
                    </div>
                    <div class="col-md-12 text-right">
                      <input class="btn btn-default" type="submit" value="Update"></input>
                    </div>
                  </div>
                </div>
                <!-- Save Button -->
              </form>
            </div>
          </div>
        </div>
      </div>
@endsection
@if(!empty($message))
@section('icon')
'ti-user'
@endsection
@section('message')
"<?php echo $message;?>"
@endsection
@section('barcolor')
'danger'
@endsection
@endif
