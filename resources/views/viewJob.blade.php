@extends('layouts.website')
@section('content')
<section class="section section-shaped section-lg">
      <div class="shape shape-style-1 bg-gradient-gray">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <?php
        $job = \App\Career::find($id);
        ?>
      </div>
      <div class="container pt-lg-md">
        <div class="row justify-content-center">
          <div class="col-lg-9">
            <div class="card bg-secondary shadow border-0">
              <div class="card-header bg-white">
                <div class="text-muted text-center">
                  <h3 class="heading">{{$job->title}}<h3>
                </div>
              </div>
              <div class="card-body py-lg-5 px-lg-5">
                <div class="row">
                  <div class="col-md-6">
                    <p class="card-text"><b>Area:</b></p>
                    <p class="card-text"><b>Experience:</b></p>
                    <p class="card-text"><b>Number Of Positions:</b></p>
                    <p class="card-text"><b>Education:</b></p>
                    <p class="card-text"><b>Upload Date:</b></p>
                    <p class="card-text"><b>Due Date:</b></p>
                    <p class="card-text"><b>Job Description:</b></p>
                  </div>
                  <div class="col-md-6">
                    <p class="card-text">{{$job->area}}</p>
                    <p class="card-text">{{$job->experience}}</p>
                    <p class="card-text">{{$job->noOfPosition}}</p>
                    <p class="card-text">{{$job->education}}</p>
                    <p class="card-text">{{$job->created_at}}</p>
                    <p class="card-text">{{$job->dueDate}}</p>
                  </div>
                  <div class="col-md-12">
                    <p class="card-text">
                    <?php
                    $descriptionUrl = "/career/"."/".$job->descriptionPath;
                    $description = Storage::get($descriptionUrl);
                    ?>
                    {!!html_entity_decode($description)!!}
                    </p>
                  </div>
                  <div class="col-md-12 mt-2">
                    <a class="btn btn-default btn-block" href="/applicationForm/<?php echo $job->id ?>">Apply Now</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection
