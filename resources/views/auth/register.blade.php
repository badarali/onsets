@extends('layouts.website')
@section('content')
<section class="section section-shaped section-lg">
  <div class="shape shape-style-1 bg-gradient-gray">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <div class="container pt-lg-md">
    <div class="row row-grid justify-content-center">
      <div class="col-lg-6">
        <div class="card bg-secondary shadow border-0">
          <div class="card-header bg-white">
            <div class="text-muted text-center">
              <h3 class="heading">Register<h3>
            </div>
          </div>


          <div class="card-body">
            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                @csrf

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="name" class="col-form-label text-md-right">{{ __('Name') }}</label>
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <input id="name" placeholder="Name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    </div>

                    <!-- @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif -->

                    @if ($errors->has('name'))
                    <div class="alert alert-danger ">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                        <strong>Sorry!</strong> {{  $errors->first('name') }}
                        </div>
                    @endif
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                      </div>
                      <input id="email" placeholder="Email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                    </div>

                    <!-- @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif -->
                    @if ($errors->has('email'))
                    <div class="alert alert-danger ">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                        <strong>Sorry!</strong> {{  $errors->first('email') }}
                        </div>
                    @endif
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                      </div>
                      <input id="password" placeholder="Password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    </div>

                    <!-- @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif -->
                    @if ($errors->has('password'))
                    <div class="alert alert-danger ">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                        <strong>Sorry!</strong> {{  $errors->first('password') }}
                        </div>
                    @endif

                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="password-confirm" class="col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-check-circle"></i></span>
                      </div>
                      <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                  </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-default btn-block">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
