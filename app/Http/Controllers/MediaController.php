<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Media;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use File;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
      return view('career.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('career.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('career.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return view('career.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      return redirect::to('/careers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      $media = \App\Media::find($id);
      $forId = $request->get('forId');
      $type = $request->get('type');
      if($media->type == 'image'){
        $path = $type.'/images/'.$media->name;
        File::delete($path);
      }
      elseif($media->type == 'video'){
        $path = $type.'/videos/'.$media->name;
        File::delete($path);
      }

      $media->delete();
      if($type=='product'){
        $typeForUrl = "products";
      }
      elseif($type=='story'){
        $typeForUrl = "stories";
      }
      elseif($type=='project'){
        $typeForUrl = "projects";
      }
      elseif($type=='aboutus'){
        $typeForUrl = "/ourinfo";
        return redirect::to($typeForUrl);
      }
      elseif($type=='slider'){

        $typeForUrl = "sliders";
      }
      $url = '/'.$typeForUrl.'/'.$forId.'/edit';
      
      return redirect::to($url);
    }
}
