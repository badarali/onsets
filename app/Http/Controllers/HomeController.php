<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use \App\FrontPage;
use File;
use \App\Ourinformation;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('project.index');
    }

    public function ourinfo(){

      //$info= FrontPage::first();
      $info = DB::table('ourinformations')->first();
      return view('ourinformation', compact('info'));

    }
    public function ourinfoAdd(Request $request){
      $frontPage= new ourinformation;
      $frontPage->phoneNO   =$request->get('phone');
      $frontPage->email     =$request->get('email');
      $frontPage->address   =$request->get('address');
      $frontPage->facebook  =$request->get('facebook');
      $frontPage->twitter   =$request->get('twitter');
      $frontPage->linkedIn  =$request->get('linkenIn');
      $frontPage->gitHub    =$request->get('gitHub');
      $frontPage->googlePlus=$request->get('goolePlus');
      $frontPage->location  =$request->get('location');
      $aboutus = $request->get('editor1');
      $aboutusDescriptionFile = time();
      $aboutusDescriptionUrl = "/aboutus/".$aboutusDescriptionFile.".txt";
      Storage::put($aboutusDescriptionUrl, $aboutus);
      $frontPage->aboutus = $aboutusDescriptionFile.".txt";
      $frontPage->save();
      $color = "success";
      $destinationImages='aboutus/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      $destinationVideo='aboutus/videos';
      $allowedVideofileExtension=['mp4','flv','m4p','mpg','mpeg','webm','mov'];
      if($request->hasFile('image')){
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $checkVideo=in_array($extension,$allowedVideofileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $media = new \App\Media;
          $file->move($destinationImages,$filename);
          $media->name = $filename;
          $media->type = "image";
          $media->for = "aboutus";
          $media->forId = $frontPage->id;
          $media->status = "Active";
          $media->save();
          $color = "success";
        }
        elseif($checkVideo){
          $media = new \App\Media;
          $file->move($destinationVideo,$filename);
          $media->name = $filename;
          $media->type = "video";
          $media->for = "aboutus";
          $media->forId = $frontPage->id;
          $media->status = "Active";
          $media->save();
          $color = "success";
        }
        else{
          $message = "Only JPEG, JPG, PNG formats allowed for images!";
          $color = "danger";
        }
      }
      return Redirect::to('ourinfo');
    }

    public function ourinfoUpdate(Request $request, $id){
      $frontPage = Ourinformation::find($id);
      $frontPage->phoneNO   =$request->get('phone');
      $frontPage->email     =$request->get('email');
      $frontPage->address   =$request->get('address');
      $frontPage->facebook  =$request->get('facebook');
      $frontPage->twitter   =$request->get('twitter');
      $frontPage->linkedIn  =$request->get('linkenIn');
      $frontPage->gitHub    =$request->get('gitHub');
      $frontPage->googlePlus=$request->get('goolePlus');
      $frontPage->location  =$request->get('location');
      $aboutus = $request->get('editor1');
      $aboutusUrl = "/aboutus/".$frontPage->aboutus;
      Storage::put($aboutusUrl, $aboutus);
      $frontPage->save();
      $color = "success";
      $destinationImages='aboutus/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      $destinationVideo='aboutus/videos';
      $allowedVideofileExtension=['mp4','flv','m4p','mpg','mpeg','webm','mov'];
      if($request->hasFile('image')){
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $checkVideo=in_array($extension,$allowedVideofileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $media = \App\Media::where('for', 'aboutus')->where('forId', $frontPage->id)->get();
          foreach ($media as $key => $value) {
            if($value->type == 'image'){
              $path = 'aboutus/images/'.$value->name;
              File::delete($path);
            }
            elseif($value->type == 'video'){
              $path = 'aboutus/videos/'.$value->name;
              File::delete($path);
            }
            $value->delete();
          }
          $media = new \App\Media;
          $file->move($destinationImages,$filename);
          $media->name = $filename;
          $media->type = "image";
          $media->for = "aboutus";
          $media->forId = $frontPage->id;
          $media->status = "Active";
          $media->save();
          $color = "success";
        }
        elseif($checkVideo){
          $media = \App\Media::where('for', 'aboutus')->where('forId', $frontPage->id)->get();
          foreach ($media as $key => $value) {
            if($value->type == 'image'){
              $path = 'aboutus/images/'.$value->name;
              File::delete($path);
            }
            elseif($value->type == 'video'){
              $path = 'aboutus/videos/'.$value->name;
              File::delete($path);
            }
            $value->delete();
          }
          $media = new \App\Media;
          $file->move($destinationVideo,$filename);
          $media->name = $filename;
          $media->type = "video";
          $media->for = "aboutus";
          $media->forId = $frontPage->id;
          $media->status = "Active";
          $media->save();
          $color = "success";
        }
        else{
          $message = "Only JPEG, JPG, PNG formats allowed for images!";
          $color = "danger";
        }
      }
      return Redirect::to('ourinfo');
    }

    public function viewAllStories(){
      return view('viewAllStories');
    }
}
