<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Product;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use File;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
        return view('product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $product = new Product;
      $title = $request->get('title');
      $url = $request->get('url');
      $status = $request->get('status');
      $description = $request->get('editor1');
      $product->productName = $title;
      $product->productURL = $url;
      $product->uploaderID = Auth::id();
      $product->status = $status;

      $destinationImages='product/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      $destinationVideo='product/videos';
      $allowedVideofileExtension=['mp4','flv','m4p','mpg','mpeg','webm','mov'];
      $productDescriptionFile = time();
      $productDescriptionUrl = "/product/".$productDescriptionFile.".txt";
      Storage::put($productDescriptionUrl, $description);
      $product->description = $productDescriptionFile.".txt";

      if($request->hasFile('image')){
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $checkVideo=in_array($extension,$allowedVideofileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $image = Image::make($file);
          //$image->resize(500, 500);
          $url = $destinationImages.'/'.$filename;

          if (!file_exists($destinationImages)) {
            mkdir($destinationImages, 666, true);
          }

          $image->save($url);
          $product->productImage = $filename;
        }
        elseif($checkVideo){
          $file->move($destinationVideo,$filename);
          $product->productVideo = $filename;
        }
        else{
          $message = "Only JPEG, JPG, PNG formats allowed for images!";
          $color = "danger";
          return view("product.create", compact('message', 'title', 'status', 'url', 'description', 'color'));
        }
        $product->save();
        $color = "success";
      }

      if($request->hasFile('images')){
        $files=Input::file("images");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $image = Image::make($file);
            $image->resize(500, 500);
            $media = new \App\Media;
            $url = $destinationImages.'/'.$filename;

            if (!file_exists($destinationImages)) {
              mkdir($destinationImages, 666, true);
            }
            

            $image->save($url);
            $media->name = $filename;
            $media->type = "image";
            $media->for = "product";
            $media->forId = $product->id;
            $media->status = "Active";
            $media->save();
          }
          else{
            $message = "Only JPEG, JPG, PNG formats allowed for images!";
            $color = "danger";
            return view("product.create", compact('message', 'title', 'status', 'url', 'description', 'color'));
          }
        }
      }
      if($request->hasFile('videos')){
        $files=Input::file("videos");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkVideo=in_array($extension,$allowedVideofileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkVideo){
            $media = new \App\Media;
            $file->move($destinationVideo,$filename);
            $media->name = $filename;
            $media->type = "video";
            $media->for = "product";
            $media->forId = $product->id;
            $media->status = "Active";
            $media->save();
          }
          else{
            $message = "Only MP4, M4P, MOV, WEBM, MPEG, FLV formats allowed for videos!";
            $color = "danger";
            return view("product.create", compact('message', 'title', 'status', 'url', 'description', 'color'));
          }
        }
      }
      $message = "Product Saved";
      return view('product.index', compact('message', 'color'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $product = \App\Product::find($id);
      return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('product.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $product = Product::find($id);
      $title = $request->get('title');
      $url = $request->get('url');
      $status = $request->get('status');
      $description = $request->get('editor1');
      $product->productName = $title;
      $product->productURL = $url;
      $product->uploaderID = Auth::id();
      $product->status = $status;
      $destinationImages='product/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      $destinationVideo='product/videos';
      $allowedVideofileExtension=['mp4','flv','m4p','mpg','mpeg','webm','mov'];
      $productDescriptionUrl = "/product/".$product->description;
      Storage::put($productDescriptionUrl, $description);
      $product->save();
      $color = "success";
      if($request->hasFile('image')){
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $checkVideo=in_array($extension,$allowedVideofileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          if(!empty($product->productImage)){
            $path = 'product/images/'.$product->productImage;
            File::delete($path);
            $product->productImage = null;
          }
          elseif(!empty($product->productVideo)){
            $path = 'product/videos/'.$product->productVideo;
            File::delete($path);
            $product->productVideo = null;
          }
          $image = Image::make($file);
          //$image->resize(500, 500);
          $url = $destinationImages.'/'.$filename;
          $image->save($url);
          $product->productImage = $filename;
        }
        elseif($checkVideo){
          if(!empty($product->productImage)){
            $path = 'product/images/'.$product->productImage;
            File::delete($path);
            $product->productImage = null;
          }
          elseif(!empty($product->productVideo)){
            $path = 'product/videos/'.$product->productVideo;
            $product->productVideo = null;
            File::delete($path);
          }
          $file->move($destinationVideo,$filename);
          $product->productVideo = $filename;
        }
        else{
          $message = "Only JPEG, JPG, PNG formats allowed for images!";
          $color = "danger";
          return view("product.create", compact('message', 'title', 'status', 'url', 'description', 'color'));
        }
        $product->save();
        $color = "success";
      }
      if($request->hasFile('images')){
        $files=Input::file("images");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $image = Image::make($file);
            $image->resize(500, 500);
            $media = new \App\Media;
            $url = $destinationImages.'/'.$filename;
            $image->save($url);
            $media->name = $filename;
            $media->type = "image";
            $media->for = "product";
            $media->forId = $product->id;
            $media->status = "Active";
            $media->save();
            $color = "success";
          }
          else{
            $color = "danger";
            $message = "Only JPEG, JPG, PNG formats allowed for images!";
            return view("product.edit", compact('message', 'title', 'status', 'url', 'description', 'color'));
          }
        }
      }
      if($request->hasFile('videos')){
        $files=Input::file("videos");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkVideo=in_array($extension,$allowedVideofileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkVideo){
            $media = new \App\Media;
            $file->move($destinationVideo,$filename);
            $url = $destinationVideo.'/'.$filename;
            $media->name = $filename;
            $media->type = "video";
            $media->for = "product";
            $media->forId = $product->id;
            $media->status = "Active";
            $media->save();
            $color = "success";
          }
          else{
            $color = "danger";
            $message = "Only MP4, M4P, MOV, WEBM, MPEG, FLV formats allowed for videos!";
            return view("product.edit", compact('message', 'title', 'status', 'url', 'description', 'color'));
          }
        }
      }
      $message = "Product Updated!";
      return view('product.index', compact('message', 'color'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = \App\Product::find($id);
        if(!empty($product->productImage)){
          $path = 'product/images/'.$product->productImage;
          File::delete($path);
        }

        if($product->productVideo){
          $path = 'product/videos/'.$product->productVideo;
          File::delete($path);
        }

        $descriptionFile=$product->description;
        Storage::delete('product/'.$descriptionFile);

        $media = \App\Media::where('for', 'product')->where('forId', $id)->get();
        foreach ($media as $key => $value) {
          if($value->type == 'image'){
            $path = 'product/images/'.$value->name;
            File::delete($path);
          }
          elseif($value->type == 'video'){
            $path = 'product/videos/'.$value->name;
            File::delete($path);
          }
          $value->delete();
        }

        $color = "danger";
        $message = "Product Deleted!";
        $product->delete();
        return view('product.index', compact('color', 'message'));
    }
}
