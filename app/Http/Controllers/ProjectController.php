<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use \App\Project;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
        return view('project.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $project =new Project;
      $project->uploaderID    =Auth::id();
      $project->title         =$request->name;
      $project->URL           =$request->link;
      $project->projectOwner  =$request->projectOwner;
      $project->status        =$request->status;
      $project->collaborationDate=$request->collabrationDate;
      $description     =$request->editor1;
      $comments =$request->comments;
      $destinationImages='project/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      $destinationVideo='project/videos';
      $allowedVideofileExtension=['mp4','flv','m4p','mpg','mpeg','webm','mov'];

      $projectDescriptionFile = time();
      $projectDescriptionUrl = "/project/".$projectDescriptionFile.".txt";
      Storage::put($projectDescriptionUrl, $description);
      $project->description= $projectDescriptionFile.".txt";

      $projectCommentFile = time();
      $projectCommentUrl = "/project-comments/".$projectCommentFile.".txt";
      Storage::put($projectCommentUrl, $comments);
      $project->projectOwnerComment	=  $projectCommentFile.".txt";

      if($request->hasFile('image')){
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $checkVideo=in_array($extension,$allowedVideofileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $image = Image::make($file);
          $image->resize(500, 500);
          $url = $destinationImages.'/'.$filename;

          if (!file_exists($destinationImages)) {
            mkdir($destinationImages, 666, true);
          }

          $image->save($url);
          $project->projectImage = $filename;
        }
        elseif($checkVideo){
          $file->move($destinationVideo,$filename);
          $project->$projectVideo = $filename;
        }
        else{
          $message = "Only JPEG, JPG, PNG formats allowed for images!";
          return view("project.create", compact('message', 'title', 'status', 'url', 'description'));
        }
        $project->save();
        $color = "success";
      }
      if($request->hasFile('images')){
        $files=Input::file("images");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $image = Image::make($file);
            $image->resize(500, 500);
            $media = new \App\Media;
            $url = $destinationImages.'/'.$filename;

            if (!file_exists($destinationImages)) {
              mkdir($destinationImages, 666, true);
            }
            
            $image->save($url);
            $media->name = $filename;
            $media->type = "image";
            $media->for = "project";
            $media->forId = $project->id;
            $media->status = "Active";
            $media->save();
          }
          else{
            $message = "Only JPEG, JPG, PNG formats allowed for images!";
            $color = "danger";
            return view("project.create", compact('message', 'title', 'status', 'url', 'description', 'color'));
          }
        }
      }
      if($request->hasFile('videos')){
        $files=Input::file("videos");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkVideo=in_array($extension,$allowedVideofileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkVideo){
            $media = new \App\Media;
            $file->move($destinationVideo,$filename);
            $media->name = $filename;
            $media->type = "video";
            $media->for = "project";
            $media->forId = $project->id;
            $media->status = "Active";
            $media->save();
          }
          else{
            $message = "Only MP4, M4P, MOV, WEBM, MPEG, FLV formats allowed for videos!";
            $color = "danger";
            return view("project.create", compact('message', 'title', 'status', 'url', 'description', 'color'));
          }
        }
      }

      $message = "Project Saved";
      $color='success';
      return view('project.index', compact('message','color'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('project.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return view('project.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $project = Project::find($id);
      $project->uploaderID    = Auth::id();
      $project->title         =$request->name;
      $project->URL           =$request->link;
      $project->projectOwner  =$request->projectOwner;
      $project->status        =$request->status;
      $description     =$request->editor1;
      $comments =$request->comments;
      $destinationImages='project/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      $destinationVideo='project/videos';
      $allowedVideofileExtension=['mp4','flv','m4p','mpg','mpeg','webm','mov'];

      if(!empty($request->hasFile('image'))){
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $checkVideo=in_array($extension,$allowedVideofileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          if(!empty($project->projectImage)){
            $path = 'project/images/'.$project->projectImage;
            File::delete($path);
            $project->projectImage = null;
          }
          elseif(!empty($project->projectVideo)){
            $path = 'project/videos/'.$project->projectVideo;
            File::delete($path);
            $project->projectVideo = null;
          }
          $image = Image::make($file);
          $image->resize(500, 500);
          $url = $destinationImages.'/'.$filename;
          $image->save($url);
          $project->projectImage = $filename;
        }
        elseif(!empty($project->projectImage)){
          if(!empty($project->projectImage)){
            $path = 'project/images/'.$project->projectImage;
            File::delete($path);
            $project->projectImage = null;
          }
          elseif(!empty($project->projectVideo)){
            $path = 'project/videos/'.$project->projectVideo;
            File::delete($path);
            $project->projectVideo = null;
          }
          $file->move($destinationVideo,$filename);
          $project->projectVideo = $filename;
        }
        else{
          $message = "Only JPEG, JPG, PNG formats allowed for images!";
          $color = "danger";
          return view("project.create", compact('message', 'title', 'status', 'url', 'description', 'danger'));
        }
      }
      if(!empty($request->collabrationDate)){
        $project->collaborationDate=$request->collabrationDate;
      }
      else{
        $project->collaborationDate=$project->collaborationDate;
      }

      if($request->hasFile('images')){
        $files=Input::file("images");
        foreach ($files as $key => $file) {
          $image = Image::make($file);
          $image->resize(500, 500);
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $media = new \App\Media;
            $url = $destinationImages.'/'.$filename;
            $image->save($url);
            $media->name = $filename;
            $media->type = "image";
            $media->for = "project";
            $media->forId = $project->id;
            $media->status = "Active";
            $media->save();
            $color = "success";
          }
          else{
            $color = "danger";
            $message = "Only JPEG, JPG, PNG formats allowed for images!";
            return view("project.edit", compact('message', 'title', 'status', 'url', 'description', 'color'));
          }
        }
      }
      if($request->hasFile('videos')){
        $files=Input::file("videos");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkVideo=in_array($extension,$allowedVideofileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkVideo){
            $media = new \App\Media;
            $file->move($destinationVideo,$filename);
            $url = $destinationVideo.'/'.$filename;
            $media->name = $filename;
            $media->type = "video";
            $media->for = "project";
            $media->forId = $project->id;
            $media->status = "Active";
            $media->save();
            $color = "success";
          }
          else{
            $color = "danger";
            $message = "Only MP4, M4P, MOV, WEBM, MPEG, FLV formats allowed for videos!";
            return view("project.edit", compact('message', 'title', 'status', 'url', 'description', 'color'));
          }
        }
      }
      $descriptionFile=$project->description;
      Storage::delete('project/'.$descriptionFile);

      $commentFile=$project->projectOwnerComment;
      Storage::delete('project-comments/'.$commentFile);

      $projectDescriptionFile = time();
      $projectDescriptionUrl = "/project/".$projectDescriptionFile.".txt";
      Storage::put($projectDescriptionUrl, $description);
      $project->description= $projectDescriptionFile.".txt";

      $projectCommentFile = time();
      $projectCommentUrl = "/project-comments/".$projectCommentFile.".txt";
      Storage::put($projectCommentUrl, $comments);
      $project->projectOwnerComment	=  $projectCommentFile.".txt";

      if($project->save()){
        $message = "Project Update";
        $color='success';
        return view('project.index' , compact('message','color'));

      }
      else{
        $message = "Project not  Update";
        $color='danger';
        return view('project.index' , compact('message','color'));
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
      $project = Project::find($id);

      if($project->projectImage){
        $path = 'project/images/'.$project->projectImage;
        File::delete($path);
      }

      if($project->projectVideo){
        $pathV = 'project/videos/'.$project->projectVideo;
        File::delete($pathV);
      }

      $descriptionFile=$project->description;
      Storage::delete('project/'.$descriptionFile);

      $commentFile=$project->projectOwnerComment;
      Storage::delete('project-comments/'.$commentFile);

      $media = \App\Media::where('for', 'project')->where('forId', $id)->get();
      foreach ($media as $key => $value) {
        if($value->type == 'image'){
          $path = 'project/images/'.$value->name;
          File::delete($path);
        }
        elseif($value->type == 'video'){
          $path = 'project/videos/'.$value->name;
          File::delete($path);
        }
        $value->delete();
      }

      if($project->delete()){
        $message = "Project Delete";
        $color='success';
        return view('project.index' , compact('message','color'));
      }
      else{
        $message = "Project not Delete";
        $color='danger';
        return view('project.index' , compact('message','color'));
      }

    }
}
