<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Role;
use Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
     
    public function index()
    {
        return view('role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $role=new Role;
          $role->uploaderID    =Auth::id();
          $role->authority    =$request->authority;
          $role->status    =$request->status;

          $alreadyRole=Role::where('authority', '=', $request->authority)->get();
          if(sizeof($alreadyRole) > 0){
            $message = "This Role have alredy Exist";
            $color='danger';
            return view('role.create', compact('message','color'));
        }
        else{
          if($role->save()){
            $message = "Role Saved";
            $color='success';
            return view('role.index', compact('message','color'));
          }
          else{
            $message = "Role not Saved";
            $color='danger';
            return view('role.index', compact('message','color'));
          }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role=Role::find($id);
        return view('role.show',compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $role=Role::find($id);
      return view('role.edit',compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role=Role::find($id);
        $role->authority    =$request->authority;
        $role->status    =$request->status;
        if($role->save()){
          $message = "Role Update";
          $color='success';
          return view('role.index', compact('message','color'));
        }
        else{
          $message = "Role not Update";
          $color='danger';
          return view('role.index', compact('message','color'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $role=Role::find($id);
          if($role->delete())
          {
            $message = "Role Delete";
            $color='success';
            return view('role.index' , compact('message','color'));

          }
          else{
            $message = "Role Delete";
            $color='danger';
            return view('role.index' , compact('message','color'));
          }

    }
}
