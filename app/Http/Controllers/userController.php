<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Contact;
use App\Address;
class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
      $data = User::all()->sortByDesc("id");
      return view('user.index',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $user = new User;
      $conatact = new Contact;
      $address = new Address;

        $user->name = $request->name;
         $user->email = $request->email;
         $user->password = Hash::make($request->password);
         $user->RoleID = $request->role;
         $user->status  =$request->status;

         $checkEmail=User::where('email', '=', $request->email)->get();
         if(sizeof($checkEmail) > 0){
           $message = "Email already Use";
           $color='danger';
           return view('user.create', compact('message','color'));
            }
          else{

         if($user->save()){
          $insertedId = $user->id;
          $conatact->contactOF = $request->contactType;
          $conatact->contactNumber = $request->Contact;
          $conatact->user_id = $insertedId;


          $insertedId = $user->id;
          $address->addressOF = $request->addressType;
          $address->addressBody = $request->address;
          $address->user_id = $insertedId;
          if($address->save() and $conatact->save()){
            $data = User::all()->sortByDesc("id");
            $message = "User Saved";
            $color='success';
            return view('user.index', compact('message','color','data'));

          }
          else{
            $data = User::all()->sortByDesc("id");
            $message = "User not Saved";
            $color='danger';
            return view('user.index', compact('message','color','data'));

          }

        }
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data=User::find($id);
      return view('user.show' , compact('message','color','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data=User::find($id);
      return view('user.edit' , compact('message','color','data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
         $user->email = $request->email;
         $user->password = Hash::make($request->password);
         $user->RoleID = $request->role;
         $user->status  =$request->status;



         if($user->save()){
           $data = User::all()->sortByDesc("id");
           $message = "User Update";
           $color='success';
           return view('user.index', compact('message','color','data'));
         }

          else{
            $data = User::all()->sortByDesc("id");
            $message = "User  not Update";
            $color='danger';
            return view('user.index', compact('message','color','data'));

          }
    }
    public function infoAddContact(Request $request){

        $conatact = new Contact;
        $conatact->contactOF = $request->contactType;
        $conatact->contactNumber = $request->Contact;
        $conatact->user_id = $request->userID;
        if($conatact->save()){
          $data = User::find($request->userID);
          $message = "User Contact Saved";
          $color='success';
          return view('user.show', compact('message','color','data'));
        }
        else{
            $data = User::find($request->userID);
          $message = "User Contact not Saved";
          $color='danger';
          return view('user.show', compact('message','color','data'));

        }
    }
    public function infoAddAddress(Request $request){
        $address = new Address;
        $address->addressOF = $request->addressType;
        $address->addressBody = $request->address;
        $address->user_id = $request->userID;
        if($address->save()){
            $data = User::find($request->userID);
          $message = "User Address Saved";
          $color='success';
          return view('user.show', compact('message','color','data'));

        }
        else{
            $data = User::find($request->userID);
          $message = "User Address not Saved";
          $color='danger';
          return view('user.show', compact('message','color','data'));
        }
    }
    public function EditContactView($id,$contact){

        $data=Contact::find($contact);
        return view('userInfo.contactEdit' , compact('data','id'));
    }
    public function EditAddressView($id, $address){
        $data=Address::find($address);
        return view('userInfo.AddressEdit' , compact('data','id'));
    }
    public function UpdateContact(Request $request, $id){

        $conatact=Contact::find($id);
        $conatact->contactOF = $request->contactType;
        $conatact->contactNumber = $request->Contact;
        if($conatact->save()){
          $data = User::find($request->userID);
          $message = "User Caontact Update";
          $color='success';
          return view('user.show', compact('message','color','data'));
        }
        else{
            $data = User::find($request->userID);
          $message = "User Contact not Update";
          $color='danger';
          return view('user.show', compact('message','color','data'));
        }
    }

    public function updateAddress(Request $request, $id){

        $address=Address::find($id);
        $address->addressOF = $request->addressType;
        $address->addressBody = $request->address;
        if($address->save()){
            $data = User::find($request->userID);
          $message = "User Address Update";
          $color='success';
          return view('user.show', compact('message','color','data'));

        }
        else{
         $data = User::find($request->userID);
          $message = "User Address not Update";
          $color='danger';
          return view('user.show', compact('message','color','data'));
        }
    }


    public function infoDeleteContact($id,$contact){

      $contact=Contact::find($contact);
      if($contact->delete())
      {
        $data = User::find($id);

        $message = "User Contact Delete";
        $color='success';
        return view('user.show' , compact('message','color','data'));

      }
      else{
        $data = User::find($id);
        $message = "User Contact not Delete";
        $color='danger';
        return view('user.show' , compact('message','color','data'));
      }

    }

    public function infoDeleteAddress($id, $address){


      $address=Address::find($address);
      if($address->delete())
      {
        $data = User::find($id);
        $message = "User Contact Delete";
        $color='success';
        return view('user.show' , compact('message','color','data'));

      }
      else{
        $data = User::find($id);
        $message = "User Contact not Delete";
        $color='danger';
        return view('user.show' , compact('message','color','data'));
      }

    }
    public function AddContactView($id){

        $data=User::find($id);
        return view('userInfo.contactAdd' , compact('data'));
    }
    public function AddAddressView($id){
      $data=User::find($id);
      return view('userInfo.AddressAdd' , compact('data'));
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user=User::find($id);
      if($user->delete())
      {
        $data = User::all()->sortByDesc("id");
        $message = "User Delete";
        $color='success';
        return view('user.index' , compact('message','color','data'));

      }
      else{
        $data = User::all()->sortByDesc("id");
        $message = "User not Delete";
        $color='danger';
        return view('user.index' , compact('message','color','data'));
      }

    }
}
