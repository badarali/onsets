<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
        return view('slider.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $slider = new \App\Slider;
      $content = $request->get('content');
      $sliderContentFile = time();
      $sliderContentUrl = "/slider/".$sliderContentFile.".txt";
      Storage::put($sliderContentUrl, $content);
      $slider->content = $sliderContentFile.".txt";
      $slider->status = $request->get('status');

      $destinationImages='slider/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      $destinationVideo='slider/videos';
      $allowedVideofileExtension=['mp4','flv','m4p','mpg','mpeg','webm','mov'];
      $slider->uploaderID = Auth::id();
      $slider->save();
      if($request->hasFile('file')){
        $file=Input::file("file");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $checkVideo=in_array($extension,$allowedVideofileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $image = Image::make($file);
          $image->resize(500, 500);
          $media = new \App\Media;
          $url = $destinationImages.'/'.$filename;
          if (!file_exists($destinationImages)) {
            mkdir($destinationImages, 666, true);
          }
          $image->save($url);
          $media->name = $filename;
          $media->type = "image";
          $media->for = "slider";
          $media->forId = $slider->id;
          $media->status = "Active";
          $media->save();
        }
        elseif($checkVideo){
          $media = new \App\Media;
          $file->move($destinationVideo,$filename);
          $media->name = $filename;
          $media->type = "video";
          $media->for = "slider";
          $media->forId = $slider->id;
          $media->status = "Active";
          $media->save();
        }
        else{
          $message = "Only JPEG, JPG, PNG formats allowed for images!";
          $color = "danger";
          return view("slider.create", compact('message', 'title', 'status', 'url', 'description', 'color'));
        }
      }
      $color = "success";
      $message = "Slider Saved!";
      return view('slider.index', compact('color', 'message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('slider.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return view('slider.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $slider = \App\Slider::find($id);
      $content = $request->get('content');
      $sliderContentFile = $slider->content;
      $sliderContentUrl = "/slider/".$sliderContentFile;
      Storage::put($sliderContentUrl, $content);
      $slider->status = $request->get('status');
      $slider->uploaderID = Auth::id();
      $slider->save();

      $destinationImages='slider/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      $destinationVideo='slider/videos';
      $allowedVideofileExtension=['mp4','flv','m4p','mpg','mpeg','webm','mov'];

      if($request->hasFile('file')){
        $file=Input::file("file");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $checkVideo=in_array($extension,$allowedVideofileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $media = \App\Media::where('for', 'slider')->where('forId', $slider->id)->first();
          if(!empty($media)){
            $path = 'slider/images/'.$media->name;
            File::delete($path);
            $media->delete();
          }
          $image = Image::make($file);
          $image->resize(500, 500);
          $media = new \App\Media;
          $url = $destinationImages.'/'.$filename;
          if (!file_exists($destinationImages)) {
            mkdir($destinationImages, 666, true);
          }
          $image->save($url);
          $media->name = $filename;
          $media->type = "image";
          $media->for = "slider";
          $media->forId = $slider->id;
          $media->status = "Active";
          $media->save();
        }
        elseif($checkVideo){
          $media = \App\Media::where('for', 'slider')->where('forId', $slider->id)->first();
          $path = 'slider/video/'.$media->name;
          File::delete($path);
          $media->delete();
          $media = new \App\Media;
          $file->move($destinationVideo,$filename);
          $media->name = $filename;
          $media->type = "video";
          $media->for = "slider";
          $media->forId = $slider->id;
          $media->status = "Active";
          $media->save();
        }
        else{
          $message = "Only JPEG, JPG, PNG formats allowed for images!";
          $color = "danger";
          return view("slider.create", compact('message', 'title', 'status', 'url', 'description', 'color'));
        }
      }
      $slider->save();
      $color = "success";
      $message = "Slider Updated";
      return view('slider.index', compact('color', 'message'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
      $slider = \App\Slider::find($id);

      $contentFile=$slider->content;
      Storage::delete('slider/'.$contentFile);

      $media = \App\Media::where('for', 'slider')->where('forId', $id)->get();
      foreach ($media as $key => $value) {
        if($value->type == 'image'){
          $path = 'slider/images/'.$value->name;
          File::delete($path);
        }
        elseif($value->type == 'video'){
          $path = 'slider/videos/'.$value->name;
          File::delete($path);
        }
        $value->delete();
      }

      if($slider->delete()){
        $message = "Slider Delete";
        $color='success';
        return view('slider.index' , compact('message','color'));
      }
      else{
        $message = "Slider not Delete";
        $color='danger';
        return view('slider.index' , compact('message','color'));
      }

    }
}
