<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\SubscribeEmail;

class SubscribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {

     }

    public function index()
    {
        $data = SubscribeEmail::all()->sortByDesc("id");
        return view('subscribeEmails', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $subcribe= new SubscribeEmail;
      $subcribe->email=$request->email;


      $SubEmails=SubscribeEmail::where('email', '=', $request->email)->get();

      if(sizeof($SubEmails) > 0){
        $message = "You have  already Subcribed";
        $color='success';
        return view('welcome', compact('message','color'));
         }
         else{

           if($subcribe->save()){
               $message = "Thank you for Subcribing";
               $color='success';
               return view('welcome', compact('message','color'));
           }
           else{
             $message = "Sorry you have not Subcribing";
             $color='danger';
             return view('welcome', compact('message','color'));
           }
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          $subEmail=SubscribeEmail::find($id);
          $status=$subEmail->status;
          if($status=="Active"){
            $changeStatus="Deactive";
          }
          else{ $changeStatus="Active"; }

          $subEmail->status    =$changeStatus;
          if($subEmail->save()){
            $data = SubscribeEmail::all()->sortByDesc("id");
            $message = "Subcribe Email Status Update";
            $color='success';
            return view('subscribeEmails', compact('message','color','data'));
          }
          else{
              $data = SubscribeEmail::all()->sortByDesc("id");
            $message = "Subcribe Email Status not Update";
            $color='danger';
            return view('subscribeEmails', compact('message','color','data'));
          }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
