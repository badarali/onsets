<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Career;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use File;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth', ['except' => ['viewJob']]);
     }

    public function index()
    {
      return view('career.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('career.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $career = new Career;
      $career->title = $request->get('title');
      $career->area = $request->get('area');
      $career->experience = $request->get('experience');
      $career->status = $request->get('status');
      $career->noOfPosition = $request->get('noOfPositions');
      $career->education = $request->get('education');
      $career->isVisible = $request->get('visible');
      $career->dueDate = $request->get('dueDate');
      $career->uploaderId = Auth::id();
      $careerDescriptionFile = time();
      $description = $request->get('editor1');
      $careerDescriptionUrl = "/career/".$careerDescriptionFile.".txt";
      Storage::put($careerDescriptionUrl, $description);
      $career->descriptionPath = $careerDescriptionFile.".txt";
      $career->save();
      $message = "Career Saved!";
      $color = "success";
      return view('career.index', compact('color', 'message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('career.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return view('career.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $career = Career::find($id);
      $career->title = $request->get('title');
      $career->area = $request->get('area');
      $career->experience = $request->get('experience');
      $career->status = $request->get('status');
      $career->isVisible = $request->get('visible');
      $career->noOfPosition = $request->get('noOfPositions');
      $career->education = $request->get('education');
      $career->dueDate = $request->get('dueDate');
      $career->uploaderId = Auth::id();
      $description = $request->get('editor1');
      $careerDescriptionUrl = "/career/".$career->descriptionPath;
      Storage::put($careerDescriptionUrl, $description);
      $career->save();
      $color = "success";
      $message = "Career Updated!";
      return view('career.index', compact('color', 'message'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $career = Career::find($id);
      $descriptionFile=$career->descriptionPath;
      Storage::delete('career/'.$descriptionFile);
      $career->delete();
      $color = "danger";
      $message = "Career Deleted";
      return view('career.index', compact('color', 'message'));
    }

    public function viewJob($id){
      return view('viewJob', compact('id'));
    }
}
