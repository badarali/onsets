<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Story;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
      return view('story.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('story.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $story=new Story;
      $story->uploaderID    =Auth::id();
      $story->publishTitle   =$request->name;
      $story->publishDate   =$request->publishDate;
      $story->visibleFront  =$request->frontVisible;
      $story->status        =$request->status;
      $description          =$request->editor1;

      $destinationImages='story/images';
      $allowedImagefileExtension=['jpg','png','jpeg'];
      $destinationVideo='story/videos';
      $allowedVideofileExtension=['mp4','flv','m4p','mpg','mpeg','webm','mov'];

      if($request->hasFile('image')){
        $file=Input::file("image");
        $extension = $file->getClientOriginalExtension();
        $checkImage=in_array($extension,$allowedImagefileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkImage){
          $image = Image::make($file);
          $image->resize(500, 500);
          $url = $destinationImages.'/'.$filename;
          if (!file_exists($destinationImages)) {
            mkdir($destinationImages, 666, true);
          }
          $image->save($url);
          $story->publishImage = $filename;
        }
        else{
          $message = "Only JPEG, JPG, PNG formats allowed for images!";
          return view("story.create", compact('message', 'title', 'status', 'url', 'description'));
        }
      }
      $storyDescriptionFile = time();
      $storyDescriptionUrl = "/story/".$storyDescriptionFile.".txt";
      Storage::put($storyDescriptionUrl, $description);
      $story->descriptionPath = $storyDescriptionFile.".txt";
      $story->save();

      if($request->hasFile('images')){
        $files=Input::file("images");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $image = Image::make($file);
            $image->resize(500, 500);
            $media = new \App\Media;
            $url = $destinationImages.'/'.$filename;
            if (!file_exists($destinationImages)) {
              mkdir($destinationImages, 666, true);
            }
            $image->save($url);
            $media->name = $filename;
            $media->type = "image";
            $media->for = "story";
            $media->forId = $story->id;
            $media->status = "Active";

            $media->save();
          }
          else{
            $message = "Only JPEG, JPG, PNG formats allowed for images!";
            $color = "danger";
            return view("story.create", compact('message', 'title', 'status', 'url', 'description', 'color'));
          }
        }
      }

      if($request->hasFile('videos')){
        $files=Input::file("videos");
        foreach ($files as $key => $file) {
          $extension = $file->getClientOriginalExtension();
          $checkVideo=in_array($extension,$allowedVideofileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkVideo){
            $media = new \App\Media;
            $file->move($destinationVideo,$filename);
            $media->name = $filename;
            $media->type = "video";
            $media->for = "story";
            $media->forId = $story->id;
            $media->status = "Active";
            
            $media->save();
          }
          else{
            $message = "Only MP4, M4P, MOV, WEBM, MPEG, FLV formats allowed for videos!";
            $color = "danger";
            return view("story.create", compact('message', 'title', 'status', 'url', 'description', 'color'));
          }
        }
      }

        $message = "Story Saved";
        $color='success';
        return view('story.index', compact('message','color'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $story = Story::find($id);
      return view('story.show', compact('story'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return view('story.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $story = Story::find($id);
        $story->uploaderID    =Auth::id();
        $story->publishTitle   =$request->name;
        $story->publishDate   =$request->publishDate;
        $story->visibleFront  =$request->frontVisible;
        $story->status        =$request->status;
        $description          =$request->editor1;
        $destinationImages='story/images';
        $allowedImagefileExtension=['jpg','png','jpeg'];
        $destinationVideo='story/videos';
        $allowedVideofileExtension=['mp4','flv','m4p','mpg','mpeg','webm','mov'];

        if($request->hasFile('image')){
          $file=Input::file("image");
          $extension = $file->getClientOriginalExtension();
          $checkImage=in_array($extension,$allowedImagefileExtension);
          $filename = time() . '-' .$file->getClientOriginalName();
          if($checkImage){
            $image = Image::make($file);
            $image->resize(500, 500);
            $path = 'story/images/'.$story->publishImage;
            File::delete($path);
            $url = $destinationImages.'/'.$filename;
            $image->save($url);
            $story->publishImage = $filename;
          }
          else{
            $message = "Only JPEG, JPG, PNG formats allowed for images!";
            return view("story.create", compact('message', 'title', 'status', 'url', 'description'));
          }
        }

        if($request->hasFile('images')){
          $files=Input::file("images");
          foreach ($files as $key => $file) {

            $extension = $file->getClientOriginalExtension();
            $checkImage=in_array($extension,$allowedImagefileExtension);
            $filename = time() . '-' .$file->getClientOriginalName();
            if($checkImage){
              $image = Image::make($file);
              $image->resize(500, 500);
              $media = new \App\Media;
              $url = $destinationImages.'/'.$filename;
              $image->save($url);
              $media->name = $filename;
              $media->type = "image";
              $media->for = "story";
              $media->forId = $story->id;
              $media->status = "Active";
              $media->save();
              $color = "success";
            }
            else{
              $color = "danger";
              $message = "Only JPEG, JPG, PNG formats allowed for images!";
              return view("story.edit", compact('message', 'title', 'status', 'url', 'description', 'color'));
            }
          }
        }
        if($request->hasFile('videos')){
          $files=Input::file("videos");
          foreach ($files as $key => $file) {
            $extension = $file->getClientOriginalExtension();
            $checkVideo=in_array($extension,$allowedVideofileExtension);
            $filename = time() . '-' .$file->getClientOriginalName();
            if($checkVideo){
              $media = new \App\Media;
              $file->move($destinationVideo,$filename);
              $url = $destinationVideo.'/'.$filename;
              $media->name = $filename;
              $media->type = "video";
              $media->for = "story";
              $media->forId = $story->id;
              $media->status = "Active";
              $media->save();
              $color = "success";
            }
            else{
              $color = "danger";
              $message = "Only MP4, M4P, MOV, WEBM, MPEG, FLV formats allowed for videos!";
              return view("story.edit", compact('message', 'title', 'status', 'url', 'description', 'color'));
            }
          }
        }

        $descriptionFile=$story->descriptionPath;
        Storage::delete('story/'.$descriptionFile);

        $storyDescriptionFile = time();
        $storyDescriptionUrl = "/story/".$storyDescriptionFile.".txt";
        Storage::put($storyDescriptionUrl, $description);
        $story->descriptionPath = $storyDescriptionFile.".txt";

        if($story->save()){
          $message = "Story Update";
          $color='success';
          return view('story.index', compact('message','color'));
        }
        else{
          $message = "Story not Update";
          $color='danger';
          return view('story.index', compact('message','color'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $story = Story::find($id);

      if($story->delete()){
        $path = 'story/images/'.$story->publishImage;
        File::delete($path);

        $descriptionFile=$story->descriptionPath;
        Storage::delete('story/'.$descriptionFile);
        $message = "Story Delete";
        $media = \App\Media::where('for', 'story')->where('forId', $id)->get();
        foreach ($media as $key => $value) {
          if($value->type == 'image'){
            $path = 'story/images/'.$value->name;
            File::delete($path);
          }
          elseif($value->type == 'video'){
            $path = 'story/videos/'.$value->name;
            File::delete($path);
          }
          $value->delete();
        }
        $color='success';
        return view('story.index', compact('message','color'));
      }
      else{
        $message = "Story not Delete";
        $color='danger';
        return view('story.index', compact('message','color'));
      }

    }
}
