<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Applicant;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use File;
use Carbon\Carbon;

class ApplicantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function __construct()
      {
          $this->middleware('auth', ['except' => ['create', 'apply', 'store']]);
      }

    public function index()
    {
      return view('applicant.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('applicant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $applicant = new Applicant;
      $name = $request->get('name');
      $address = $request->get('address');
      $contactNo = $request->get('contactNo');
      $linkedIn = $request->get('linkedIn');
      $careerId = $request->get('careerId');
      $email = $request->get('email');
      $coverLetter = $request->get('coverLetter');
      $applicant->name = $name;
      $applicant->email = $email;
      $applicant->contactNO = $contactNo;
      $applicant->address = $address;
      $applicant->linkedInProfile = $linkedIn;
      $applicant->status = 'Applied';
      $applicant->careerID = $careerId;
      if($request->hasFile('document')){
        $file=Input::file("document");
        $destinationDocuments='applicant/document';
        $allowedDocumentfileExtension=['pdf'];
        $extension = $file->getClientOriginalExtension();
        $checkDocument=in_array($extension,$allowedDocumentfileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkDocument){
          $file->move($destinationDocuments,$filename);
          $applicant->resume = $filename;
        }
        else{
          $messages = "Only PDF format allowed!";
          return view("applicant.create", compact('messages', 'name', 'email', 'coverLetter', 'contactNo', 'address', 'linkedIn', 'careerId'));
        }
      }
      $coverLetterFile = time();
      $coverLetterUrl = "/applicant/coverLetter/".$coverLetterFile.".txt";
      Storage::put($coverLetterUrl, $coverLetter);
      $applicant->coverLetter = $coverLetterFile.".txt";
      $applicant->save();
      $url = "/applicationForm/".$careerId;
      $messages = "Applied";
      $color = "success";
      $id = $applicant->careerID;
      return view('applicant.create', compact('color', 'messages', 'id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('applicant.show', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return view('applicant.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $applicant = Applicant::find($id);
      $name = $request->get('name');
      $address = $request->get('address');
      $contactNo = $request->get('contactNo');
      $linkedIn = $request->get('linkedIn');
      $email = $request->get('email');
      $coverLetter = $request->get('coverLetter');
      $applicant->name = $name;
      $applicant->email = $email;
      $applicant->contactNO = $contactNo;
      $applicant->address = $address;
      $applicant->linkedInProfile = $linkedIn;
      $applicant->status = $request->get('status');
      if($request->hasFile('document')){
        $file=Input::file("document");
        $destinationDocuments='applicant/document';
        $allowedDocumentfileExtension=['pdf'];
        $extension = $file->getClientOriginalExtension();
        $checkDocument=in_array($extension,$allowedDocumentfileExtension);
        $filename = time() . '-' .$file->getClientOriginalName();
        if($checkDocument){
          $file->move($destinationDocuments,$filename);
          $path = 'applicant/document/'.$applicant->resume;
          File::delete($path);
          $applicant->resume = $filename;
        }
        else{
          $message = "Only PDF format allowed!";
          return view("applicant.edit", compact('message', 'name', 'email', 'coverLetter', 'contactNo', 'address', 'linkedIn', 'careerId'));
        }
      }
      $coverLetterUrl = "/applicant/coverLetter/".$applicant->coverLetter;
      Storage::put($coverLetterUrl, $coverLetter);
      $applicant->save();
      $message = "Applicant Updated";
      $color = "success";
      $job_id = $applicant->careerID;
      return view('applicant.index', compact('color', 'message', 'job_id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $applicant = Applicant::find($id);
      $path = 'applicant/document/'.$applicant->resume;
      File::delete($path);

      $coverLetterFile=$applicant->coverLetter;
      Storage::delete('applicant/coverLetter/'.$coverLetterFile);
      $job_id = $applicant->careerID;
      $applicant->delete();

      $message = "Applicant Deleted";
      $color = "success";

      return view('applicant.index', compact('color', 'message', 'job_id'));
    }

    public function apply($id){
      return view('applicant.create', compact('id'));
    }

    public function viewApplicants($job_id){
      return view('applicant.index', compact('job_id'));
    }
}
