<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \App\Query;
use \App\SubscribeEmail;

class QueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {

     }

    public function index()
    {
      $query = Query::all()->sortByDesc("id");
      return view('query.index',compact('query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $query=new Query;
      $query->subject=$request->subject;
      $query->contact=$request->contact;
      $query->email=$request->email;
      $Description=$request->querydescription;


      $queryDescriptionFile = time();
      $queryDescriptionUrl = "/query/".$queryDescriptionFile.".txt";
      Storage::put($queryDescriptionUrl, $Description);
      $query->queryPath = $queryDescriptionFile.".txt";

      if($query->save()){
        $SubEmails=SubscribeEmail::where('email', '=', $request->email)->get();

        if(sizeof($SubEmails) > 0){
          $message = "Your Query has been Submitted";
          $color='success';
          return view('welcome', compact('message','color'));
           }
           else{
             $SubscribeEmail=new SubscribeEmail;
             $SubscribeEmail->email=$request->email;

             if($SubscribeEmail->save()){
               $message = "Your Query has been Submitted";
               $color='success';
               return view('welcome', compact('message','color'));
             }
           }
      }
      else{
        $message = "Your Query has not been Submitted";
        $color='danger';
        return view('welcome', compact('message','color'));
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $query = Query::find($id);
      return view('query.show', compact('query'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $query=Query::find($id);
      $query->status    =$request->status;
      if($query->save()){
        $query = Query::all()->sortByDesc("id");
        $message = "Query Status Update";
        $color='success';
        return view('query.index', compact('message','color','query'));
      }
      else{
          $query = Query::all()->sortByDesc("id");
        $message = "Query Status not Update";
        $color='danger';
        return view('query.index', compact('message','color','query'));
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function DifferentQuery($status){

       $query=Query::where('status',$status)->orderBy('id', 'desc')->get();
       return view('query.index', compact('query'));
     }
    public function destroy($id)
    {


    }
}
