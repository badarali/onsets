
$('#image').change(function() {
  console.log($('#image')[0].files.length);
  if($('#image')[0].files.length == 1){
    $('#imageLabel').text($('#image')[0].files[0].name);
  }
  else if($('#image')[0].files.length > 1){
    $('#imageLabel').text($('#image')[0].files.length + " files selected");
  }
  else if($('#image')[0].files.length == 0){
    $('#imageLabel').text("");
  }
});

$('#images').change(function() {
  if($('#images')[0].files.length == 1){
    $('#imagesLabel').text($('#images')[0].files[0].name);
  }
  else if($('#images')[0].files.length > 1){
    $('#imagesLabel').text($('#images')[0].files.length + " files selected");
  }
  else if($('#images')[0].files.length == 0){
    $('#imagesLabel').text("");
  }
});

$('#videos').change(function() {
  console.log($('#videos')[0].files.length);
  if($('#videos')[0].files.length == 1){
    $('#videosLabel').text($('#videos')[0].files[0].name);
  }
  else if($('#videos')[0].files.length > 1){
    $('#videosLabel').text($('#videos')[0].files.length + " files selected");
  }
  else if($('#videos')[0].files.length == 0){
    $('#videosLabel').text("");
  }
});
