<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/applicationForm/{id}', 'ApplicantController@apply');
Route::get('/viewJob/{id}', 'CareerController@viewJob');
Route::get('/viewAllJobs', function () {
    return view('viewAllJobs');
});
Route::get('/', function () {
  $message = "Welcome to OnSets!";
    return view('welcome', compact('message'));
});
Route::resource('/queries', 'QueryController');
Route::resource('/subcribeEmails', 'SubscribeController');
Route::get('/differnt/{status}', 'QueryController@DifferentQuery');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/ourinfo', 'HomeController@ourinfo');
Route::get('/viewAllStories', 'HomeController@viewAllStories');
Route::post('/ourinfoAdd', 'HomeController@ourinfoAdd');
Route::post('/ourinfoUpdate/{id}', 'HomeController@ourinfoUpdate');
Route::get('/viewApplicants/{id}', 'ApplicantController@viewApplicants');
Route::resource('/addresses', 'AddressController');
Route::resource('/products', 'ProductController');
Route::resource('/projects', 'ProjectController');
Route::resource('/careers', 'CareerController');
Route::resource('/stories', 'StoryController');
Route::resource('/roles', 'RoleController');
Route::resource('/applicants', 'ApplicantController');
Route::resource('/users', 'userController');
Route::resource('/contacts', 'ContactController');


Route::post('/usersContact', 'userController@infoAddContact');
Route::post('/usersAddress', 'userController@infoAddAddress');
Route::post('/usersContactD/{id}/{contact}', 'userController@infoDeleteContact');
Route::post('/usersAddressD/{id}/{address}', 'userController@infoDeleteAddress');
Route::get('/userContact/{id}', 'userController@AddContactView');
Route::get('/userAddress/{id}', 'userController@AddAddressView');
Route::get('/ContactEdit/{id}/{contact}', 'userController@EditContactView');
Route::get('/AddressEdit/{id}/{address}', 'userController@EditAddressView');
Route::post('/UpdateContact/{id}', 'userController@UpdateContact');
Route::post('/updateAddress/{id}', 'userController@updateAddress');


Route::resource('/media', 'MediaController');
Route::resource('/sliders', 'SliderController');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
