<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careers', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title');
          $table->string('area');
          $table->date('dueDate');
          $table->string('descriptionPath');
          $table->string('experience');
          $table->string('education');
          $table->boolean('isVisible');
          $table->enum('status', ['Active', 'Deactive']);
          $table->integer('uploaderId');
          $table->integer('noOfPosition')->default(1);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careers');
    }
}
