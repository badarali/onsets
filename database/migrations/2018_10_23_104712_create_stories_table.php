<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stories', function (Blueprint $table) {
            $table->increments('id');
            $table->date('publishDate');
            $table->string('publishTitle');
            $table->string('publishImage');
            $table->boolean('visibleFront')->default(1)->comment = "1 visible 0 Invisible";
            $table->string('descriptionPath');
            $table->enum('status', ['Active', 'Deactive']);
            $table->integer('uploaderID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stories');
    }
}
