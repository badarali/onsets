<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('URL');
            $table->string('projectOwner');
            $table->string('projectOwnerComment');
            $table->string('projectImage');
            $table->string('projectVideo')->nullable();
            $table->integer('uploaderID');
            $table->enum('status', ['Active', 'Deactive']);
            $table->date('collaborationDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
